﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class SettingsForm : Form
    {
        private ControlForm controlForm;

        float projectionBodyFontSizeMultpilier;
        public SettingsForm(ControlForm controlForm)
        {
            this.controlForm = controlForm;

            InitializeComponent();

            Init();
            GetSetProjectSettings();
            Post();
        }

        private void Init()
        {
            /*
             * Projection text align combo box
             */
            // Populate text align combo box
            projectionBodyLabelTextAlignComboBox.Items.Add("Top Center");
            projectionBodyLabelTextAlignComboBox.Items.Add("Top Left");
            projectionBodyLabelTextAlignComboBox.Items.Add("Top Right");
            projectionBodyLabelTextAlignComboBox.Items.Add("Middle Center");
            projectionBodyLabelTextAlignComboBox.Items.Add("Middle Left");
            projectionBodyLabelTextAlignComboBox.Items.Add("Middle Right");
            projectionBodyLabelTextAlignComboBox.Items.Add("Bottom Center");
            projectionBodyLabelTextAlignComboBox.Items.Add("Bottom Left");
            projectionBodyLabelTextAlignComboBox.Items.Add("Bottom Right");

            // Set selected text align
            projectionBodyLabelTextAlignComboBox.SelectedIndex = projectionBodyLabelTextAlignComboBox.Items.IndexOf(Properties.Settings.Default.ProjectionBodyLabelTextAlign);
        }

        private void GetSetProjectSettings()
        {
            LogI("Properties.Settings.Default.ProjectionPanelPaddingBottom: " + Properties.Settings.Default.ProjectionPanelPaddingBottom);

            projectionPanel.Padding = new Padding(
                Properties.Settings.Default.ProjectionPanelPaddingLeft,
                Properties.Settings.Default.ProjectionPanelPaddingTop,
                Properties.Settings.Default.ProjectionPanelPaddingRight,
                Properties.Settings.Default.ProjectionPanelPaddingBottom);

            projectionPanelPaddingTopNumericUpDown.Value = (decimal)Properties.Settings.Default.ProjectionPanelPaddingTop;
            projectionPanelPaddingBottomNumericUpDown.Value = (decimal)Properties.Settings.Default.ProjectionPanelPaddingBottom;
            projectionPanelPaddingRightNumericUpDown.Value = (decimal)Properties.Settings.Default.ProjectionPanelPaddingRight;
            projectionPanelPaddingLeftNumericUpDown.Value = (decimal)Properties.Settings.Default.ProjectionPanelPaddingLeft;

            projectionPanel.BackColor = Properties.Settings.Default.ProjectionPanelBackColor;
            projectionPanel.ForeColor = Properties.Settings.Default.ProjectionPanelForeColor;

            string fontFamily = Properties.Settings.Default.ProjectionFontFamily;
            if (fontFamily == "") fontFamily = this.Font.FontFamily.Name;

            float projectionPanelFontSize = Properties.Settings.Default.ProjectionPanelFontSize;
            if (projectionPanelFontSize == 0) projectionPanelFontSize = this.Font.Size;

            projectionPanel.Font = new Font(fontFamily, projectionPanelFontSize);
            projectionPanelFontSizeNumericUpDown.Value = (decimal)projectionPanelFontSize;

            projectionHeaderLabel.Font = new Font(fontFamily, projectionPanelFontSize);
            projectionFooterLabel.Font = new Font(fontFamily, projectionPanelFontSize);

            projectionHeaderLabel.Size = new Size(projectionHeaderLabel.Size.Width, (int)projectionPanelFontSize * 2);
            projectionFooterLabel.Size = new Size(projectionFooterLabel.Size.Width, (int)projectionPanelFontSize * 2);

            projectionBodyLabel.BackColor = Properties.Settings.Default.ProjectionBodyBackColor;
            projectionBodyLabel.ForeColor = Properties.Settings.Default.ProjectionBodyForeColor;

            float projectionBodyFontSize = Properties.Settings.Default.ProjectionBodyFontSize;
            if (projectionBodyFontSize == 0) projectionBodyFontSize = this.Font.Size;
            projectionBodyLabel.Font = new Font(projectionBodyLabel.Font.FontFamily, projectionBodyFontSize);

            projectionBodyFontSizeNumericUpDown.Value = (decimal)projectionBodyFontSize;

            projectionBodyFontSizeMultpilier = Properties.Settings.Default.ProjectionBodyFontSizeMultpilier;
            if (projectionBodyFontSizeMultpilier == 0) projectionBodyFontSizeMultpilier = 1;

            projectionBodyFontSizeMultpilierNumericUpDown.Value = (decimal)projectionBodyFontSizeMultpilier;

            fontFamily = Properties.Settings.Default.ProjectionFontFamily;
            if (fontFamily == "") fontFamily = this.Font.FontFamily.Name;

            projectionBodyLabel.Font = new Font(fontFamily, projectionBodyLabel.Font.Size);

            /*
             * Projection body text align
             */
            switch (Properties.Settings.Default.ProjectionBodyLabelTextAlign) {
                case "Top Center": projectionBodyLabel.TextAlign = ContentAlignment.TopCenter; break;
                case "Top Left": projectionBodyLabel.TextAlign = ContentAlignment.TopLeft; break;
                case "Top Right": projectionBodyLabel.TextAlign = ContentAlignment.TopRight; break;
                case "Middle Center": projectionBodyLabel.TextAlign = ContentAlignment.MiddleCenter; break;
                case "Middle Left": projectionBodyLabel.TextAlign = ContentAlignment.MiddleLeft; break;
                case "Middle Right": projectionBodyLabel.TextAlign = ContentAlignment.MiddleRight; break;
                case "Bottom Center": projectionBodyLabel.TextAlign = ContentAlignment.BottomCenter; break;
                case "Bottom Left": projectionBodyLabel.TextAlign = ContentAlignment.BottomLeft; break;
                case "Bottom Right": projectionBodyLabel.TextAlign = ContentAlignment.BottomRight; break;

                default:  projectionBodyLabel.TextAlign = ContentAlignment.MiddleCenter; break;
            };

            /*
             * Update projection form
             */
            controlForm.ProjectionFormUpdateUI();
        }

        private void Post()
        {
            // Populate font family combo box
            foreach (FontFamily fontFamily in new InstalledFontCollection().Families)
                projectionFontFamilyComboBox.Items.Add(fontFamily.Name);
        }

        private void pickColorButton_Click(object sender, EventArgs e)
        {
            string tag = (string)((Button)sender).Tag;

            ColorDialog MyDialog = new ColorDialog();
            MyDialog.AllowFullOpen = false;
            MyDialog.ShowHelp = false;
            MyDialog.Color = projectionBodyLabel.BackColor;
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                if (tag == "projectionBodyBackgroundColor")
                {
                    projectionBodyLabel.BackColor = MyDialog.Color;
                    Properties.Settings.Default.ProjectionBodyBackColor = MyDialog.Color;
                }

                if (tag == "projectionBodyForeColor")
                {
                    projectionBodyLabel.ForeColor = MyDialog.Color;
                    Properties.Settings.Default.ProjectionBodyForeColor = MyDialog.Color;
                }

                if (tag == "projectionPanelBackColor")
                {
                    projectionPanel.BackColor = MyDialog.Color;
                    Properties.Settings.Default.ProjectionPanelBackColor = MyDialog.Color;
                }

                if (tag == "projectionPanelForeColor")
                {
                    projectionPanel.BackColor = MyDialog.Color;
                    Properties.Settings.Default.ProjectionPanelForeColor = MyDialog.Color;
                }

                Properties.Settings.Default.Save();

                GetSetProjectSettings();
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            float value = (float)((NumericUpDown)sender).Value;
            string tag = (string)((NumericUpDown)sender).Tag;

            if (tag == "projectionBodyFontSize")
            {
                projectionBodyLabel.Font = new Font(projectionBodyLabel.Font.FontFamily, value);
                Properties.Settings.Default.ProjectionBodyFontSize = value;

                AdjustFontSizeToFit(projectionBodyLabel, value, Properties.Settings.Default.ProjectionBodyFontSizeMultpilier);
            }

            if(tag == "projectionBodyFontSizeMultpilier")
            {
                Properties.Settings.Default.ProjectionBodyFontSizeMultpilier = value;
            }

            if (tag == "projectionPanelPaddingTop")
            {
                Properties.Settings.Default.ProjectionPanelPaddingTop = (int)value;
            }

            if (tag == "projectionPanelPaddingBottom")
            {
                Properties.Settings.Default.ProjectionPanelPaddingBottom = (int)value;
            }

            if (tag == "projectionPanelPaddingRight")
            {
                Properties.Settings.Default.ProjectionPanelPaddingRight = (int)value;
            }

            if (tag == "projectionPanelPaddingLeft")
            {
                Properties.Settings.Default.ProjectionPanelPaddingLeft = (int)value;
            }

            if (tag == "ProjectionPanelFontSize")
            {
                Properties.Settings.Default.ProjectionPanelFontSize = value;
            }

            Properties.Settings.Default.Save();

            GetSetProjectSettings();
        }

        private void AdjustFontSizeToFit(Label projectionBodyLabel, float value, float multiplier)
        {
            string text = projectionBodyLabel.Text;

            float height = projectionBodyLabel.Size.Height;
            height -= (projectionBodyLabel.Padding.Top + projectionBodyLabel.Padding.Bottom);

            float width = projectionBodyLabel.Size.Width;
            width -= (projectionBodyLabel.Padding.Left + projectionBodyLabel.Padding.Right);

            float area = height * width;

            float numberOfCharacters = text.Length;
            float maxFontSize = (float)Math.Sqrt((area/ numberOfCharacters)) * multiplier;
            if(maxFontSize <= 0) maxFontSize = 1f;

            if (value > maxFontSize) projectionBodyLabel.Font = new Font(projectionBodyLabel.Font.FontFamily, maxFontSize);
        }

        /*
         * Events
         */
        private void projectionFontFamilyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ProjectionFontFamily = projectionFontFamilyComboBox.Text;
            Properties.Settings.Default.Save();

            projectionBodyLabel.Font = new Font(projectionFontFamilyComboBox.Text, projectionBodyLabel.Font.Size);

            GetSetProjectSettings();
        }

        private void projectionBodyLabelTextAlignmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ProjectionBodyLabelTextAlign = projectionBodyLabelTextAlignComboBox.Text;
            Properties.Settings.Default.Save();

            GetSetProjectSettings();
        }

        /*
         * General
         */
        private void LogI(string info)
        {
            Debug.WriteLine("SettingsForm: " + info);
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void clearBiblesButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.BibleData = JsonSerializer.Serialize(new List<BibleData>());
            Properties.Settings.Default.Save();
        }


    }
}
