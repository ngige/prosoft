﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class NextItemBibleVerseControl : UserControl
    {
        private int index;
        private ControlForm controlFrm;
        public string verse;
        // private List<Verse> verses;

        public NextItemBibleVerseControl(int index, ControlForm controlFrm, string verse/*List<Verse> verses*/)
        {
            this.index = index;
            this.controlFrm = controlFrm;
            this.verse = verse;
            // this.verses = verses;

            InitializeComponent();

            verseLabel.Text = verse;
            // verseLabel.Text = verses[0].verse;
            ResizeLabel();
        }

        public string Verse
        {
            get { return verse; }
            set {
                verse = value;
                verseLabel.Text = value;

                Debug.WriteLine("-------------*");
            }
        }

        private void ResizeLabel()
        {
            verseLabel.MaximumSize = new Size(this.Width, 0);
        }

        private void NextItemBibleVerseControl_Resize(object sender, EventArgs e)
        {
            ResizeLabel();
        }

        private void NextItemBibleVerseControl_DoubleClick(object sender, EventArgs e)
        {
            controlFrm.ProjectionPanel_AddBibleVerses(index);
        }
    }
}
