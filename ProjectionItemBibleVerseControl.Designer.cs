﻿
namespace ProSoft
{
    partial class ProjectionItemBibleVerseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // verseLabel
            // 
            this.verseLabel.AutoSize = true;
            this.verseLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verseLabel.Location = new System.Drawing.Point(0, 0);
            this.verseLabel.Name = "verseLabel";
            this.verseLabel.Size = new System.Drawing.Size(35, 13);
            this.verseLabel.TabIndex = 0;
            this.verseLabel.Text = "label1";
            this.verseLabel.Click += new System.EventHandler(this.ProjectiontemBibleVerseControl_Click);
            // 
            // ProjectionItemBibleVerseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.verseLabel);
            this.Name = "ProjectionItemBibleVerseControl";
            this.Size = new System.Drawing.Size(35, 13);
            this.Click += new System.EventHandler(this.ProjectiontemBibleVerseControl_Click);
            this.Resize += new System.EventHandler(this.ProjectiontemBibleVerseControl_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label verseLabel;
    }
}
