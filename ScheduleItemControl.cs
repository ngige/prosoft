﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class ScheduleItemControl : UserControl
    {
        public ScheduleItemControl()
        {
            InitializeComponent();
        }

        private ControlForm controlForm_;
        private int id;
        private int index;
        private int songControlListindex;
        private string title;
        private string verses;
        private string author;

        public ControlForm ControlForm_
        {
            set { controlForm_ = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public int SongControlListindex
        {
            get { return songControlListindex; }
            set { songControlListindex = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; songTitleLabel.Text = title; }
        }

        public string Verses
        {
            get { return verses; }
            set { verses = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; songTitleLabel.Text = title; songAuthorLabel.Text = author; }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlForm_.ScheduleRemoveItem(this.Index);
        }

        private void activateScheduleItemControlContextMenuStripLabel_Click(object sender, EventArgs e)
        {
            scheduleItemControlContextMenuStrip.Show(activateScheduleItemControlContextMenuStripLabel, new Point(0, 0));
        }

        private void ScheduleItemControl_DoubleClick(object sender, EventArgs e)
        {
            controlForm_.PushSongToNextItemAndProject(this.SongControlListindex);

            Debug.WriteLine("ScheduleItemControl: db click Index: "  + this.SongControlListindex);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlForm_.ShowEditSongForm(this.SongControlListindex, this.Index);
        }
    }
}
