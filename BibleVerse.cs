﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProSoft
{
    class BibleVerse
    {
        public int number;
        public string verse;

        public BibleVerse(int verseNumber, string verse)
        {
            this.number = verseNumber;
            this.verse = verse;
        }

        public int VerseNumber
        {
            get { return number; }
        }

        public string Verse
        {
            get { return verse; }
        }
    }
}
