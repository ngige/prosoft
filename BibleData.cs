﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProSoft
{
    class BibleData
    {
        public string version;
        public string location;

        public BibleData(string version, string location)
        {
            this.version = version;
            this.location = location;
        }

        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        public string Location
        {
            get { return location; }
            set { location = value; }
        }
    }
}
