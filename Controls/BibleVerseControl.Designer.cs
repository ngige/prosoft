﻿
namespace ProSoft
{
    partial class BibleVerseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // verseLabel
            // 
            this.verseLabel.AutoSize = true;
            this.verseLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.verseLabel.Location = new System.Drawing.Point(0, 0);
            this.verseLabel.MaximumSize = new System.Drawing.Size(100, 0);
            this.verseLabel.Name = "verseLabel";
            this.verseLabel.Size = new System.Drawing.Size(99, 104);
            this.verseLabel.TabIndex = 2;
            this.verseLabel.Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor in" +
    "cididunt ut labore et dolore magna aliqua.";
            this.verseLabel.Click += new System.EventHandler(this.BibleVerseControl_Click);
            this.verseLabel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.BibleVerseControl_DoubleClick);
            this.verseLabel.MouseEnter += new System.EventHandler(this.verseLabel_MouseEnter);
            this.verseLabel.MouseLeave += new System.EventHandler(this.verseLabel_MouseLeave);
            // 
            // BibleVerseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.verseLabel);
            this.Name = "BibleVerseControl";
            this.Size = new System.Drawing.Size(99, 104);
            this.Click += new System.EventHandler(this.BibleVerseControl_Click);
            this.DoubleClick += new System.EventHandler(this.BibleVerseControl_DoubleClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.BibleVerseControl_DoubleClick);
            this.MouseEnter += new System.EventHandler(this.verseLabel_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.verseLabel_MouseLeave);
            this.Resize += new System.EventHandler(this.BibleVerseControl_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label verseLabel;
    }
}
