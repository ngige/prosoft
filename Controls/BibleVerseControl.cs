﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class BibleVerseControl : UserControl
    {
        private Color originalBackColor;

        private ControlForm controlFrm;
        private Verse verse;

        private bool isActive;

        public BibleVerseControl(ControlForm controlFrm, Verse verse)
        {
            this.controlFrm = controlFrm;
            this.verse = verse;

            InitializeComponent();

            originalBackColor = this.BackColor;

            verseLabel.Text = Convert.ToString(verse.number) + ". " + verse.verse;
            ResizeControls();
        }

        private void ResizeControls()
        {
            verseLabel.MaximumSize = new Size(this.Width, 0);
            this.Height = verseLabel.Height;
        }
        public void Activate()
        {
            isActive = true;
            Highlight();
        }

        public void Deactivate()
        {
            isActive = false;
            Unhighlight();
        }

        /*
         * Events
         */
        private void BibleVerseControl_Click(object sender, EventArgs e)
        {
            controlFrm.NextItemPanel_AddBibleVerses(verse);
            controlFrm.activeBibleVerseIndex = verse.number - 1;
            Activate();
        }

        private void BibleVerseControl_Resize(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private void verseLabel_MouseEnter(object sender, EventArgs e)
        {
            if(!isActive) Highlight();
        }

        private void verseLabel_MouseLeave(object sender, EventArgs e)
        {
            if(!isActive) Unhighlight();
        }
        /*
         * Helper Methods
         */
        private void Highlight()
        {
            this.BackColor = SystemColors.ActiveCaption;
        }

        private void Unhighlight()
        {
            this.BackColor = originalBackColor;
        }

        private void BibleVerseControl_DoubleClick(object sender, EventArgs e)
        {
            Debug.WriteLine("Double Click");
            controlFrm.ExpressProjectbibleVerses();
        }

        private void BibleVerseControl_DoubleClick(object sender, MouseEventArgs e)
        {
            Debug.WriteLine("Mouse Double Click");
            controlFrm.ExpressProjectbibleVerses();
        }
    }
}
