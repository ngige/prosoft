﻿namespace ProSoft
{
    partial class SongItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.songListItemContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.projectStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.moreContextMenuLabel = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.songAuthorLabel = new System.Windows.Forms.Label();
            this.songTitleLabel = new System.Windows.Forms.Label();
            this.songListItemContextMenuStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // songListItemContextMenuStrip
            // 
            this.songListItemContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectStripMenuItem,
            this.sheduleToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.songListItemContextMenuStrip.Name = "songListItemContextMenuStrip";
            this.songListItemContextMenuStrip.Size = new System.Drawing.Size(123, 92);
            // 
            // projectStripMenuItem
            // 
            this.projectStripMenuItem.Name = "projectStripMenuItem";
            this.projectStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.projectStripMenuItem.Text = "Project";
            this.projectStripMenuItem.Click += new System.EventHandler(this.projectStripMenuItem_Click);
            // 
            // sheduleToolStripMenuItem
            // 
            this.sheduleToolStripMenuItem.Name = "sheduleToolStripMenuItem";
            this.sheduleToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.sheduleToolStripMenuItem.Text = "Schedule";
            this.sheduleToolStripMenuItem.Click += new System.EventHandler(this.sheduleToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.Controls.Add(this.moreContextMenuLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(500, 35);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // moreContextMenuLabel
            // 
            this.moreContextMenuLabel.Image = global::ProSoft.Properties.Resources.dots_vertical_black;
            this.moreContextMenuLabel.Location = new System.Drawing.Point(443, 0);
            this.moreContextMenuLabel.Name = "moreContextMenuLabel";
            this.moreContextMenuLabel.Padding = new System.Windows.Forms.Padding(8, 3, 12, 4);
            this.moreContextMenuLabel.Size = new System.Drawing.Size(20, 20);
            this.moreContextMenuLabel.TabIndex = 5;
            this.moreContextMenuLabel.Click += new System.EventHandler(this.moreContextMenuLabel_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.songAuthorLabel);
            this.panel3.Controls.Add(this.songTitleLabel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(440, 35);
            this.panel3.TabIndex = 7;
            this.panel3.DoubleClick += new System.EventHandler(this.SongItemControl_DoubleClick);
            // 
            // songAuthorLabel
            // 
            this.songAuthorLabel.AutoEllipsis = true;
            this.songAuthorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.songAuthorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.songAuthorLabel.Location = new System.Drawing.Point(0, 20);
            this.songAuthorLabel.Name = "songAuthorLabel";
            this.songAuthorLabel.Size = new System.Drawing.Size(440, 13);
            this.songAuthorLabel.TabIndex = 1;
            this.songAuthorLabel.Text = "Song author";
            this.songAuthorLabel.Click += new System.EventHandler(this.SongListItem_Click);
            this.songAuthorLabel.DoubleClick += new System.EventHandler(this.SongItemControl_DoubleClick);
            this.songAuthorLabel.MouseEnter += new System.EventHandler(this.SongListItem_MouseEnter);
            this.songAuthorLabel.MouseLeave += new System.EventHandler(this.SongListItem_MouseLeave);
            // 
            // songTitleLabel
            // 
            this.songTitleLabel.AutoEllipsis = true;
            this.songTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.songTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.songTitleLabel.Location = new System.Drawing.Point(0, 0);
            this.songTitleLabel.MinimumSize = new System.Drawing.Size(100, 0);
            this.songTitleLabel.Name = "songTitleLabel";
            this.songTitleLabel.Size = new System.Drawing.Size(440, 20);
            this.songTitleLabel.TabIndex = 0;
            this.songTitleLabel.Text = "Song title";
            this.songTitleLabel.Click += new System.EventHandler(this.SongListItem_Click);
            this.songTitleLabel.DoubleClick += new System.EventHandler(this.SongItemControl_DoubleClick);
            this.songTitleLabel.MouseEnter += new System.EventHandler(this.SongListItem_MouseEnter);
            this.songTitleLabel.MouseLeave += new System.EventHandler(this.SongListItem_MouseLeave);
            // 
            // SongItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ContextMenuStrip = this.songListItemContextMenuStrip;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SongItemControl";
            this.Size = new System.Drawing.Size(500, 36);
            this.MouseEnter += new System.EventHandler(this.SongListItem_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.SongListItem_MouseLeave);
            this.songListItemContextMenuStrip.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip songListItemContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem sheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Label moreContextMenuLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label songAuthorLabel;
        private System.Windows.Forms.Label songTitleLabel;
        private System.Windows.Forms.ToolStripMenuItem projectStripMenuItem;
    }
}
