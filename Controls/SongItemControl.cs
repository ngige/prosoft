﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ProSoft
{
    public partial class SongItemControl : UserControl
    {
        Color originalBackColor;

        public SongItemControl()
        {
            InitializeComponent();

            originalBackColor = this.BackColor;
        }

        private void SongListItem_Load(object sender, EventArgs e)
        {

        }

        private ControlForm controlForm;
        private int id;
        private int index;
        private string title;
        private string verses;
        private string author;

        public ControlForm Control
        {
            set { controlForm = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; songTitleLabel.Text = title;  }
        }

        public string Verses
        {
            get { return verses; }
            set { verses = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; songTitleLabel.Text = title; songAuthorLabel.Text = author; }
        }

        /*
         * Events
         */
        private void SongListItem_MouseEnter(object sender, EventArgs e)
        {
            originalBackColor = this.BackColor;
            this.BackColor = SystemColors.ActiveCaption;
        }

        private void SongListItem_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = originalBackColor;
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlForm.ShowEditSongForm(this.index);
        }

        private void moreContextMenuLabel_Click(object sender, EventArgs e)
        {
            songListItemContextMenuStrip.Show(moreContextMenuLabel, new Point(0, 0));
        }

        private void SongListItem_Click(object sender, EventArgs e)
        {
            controlForm.PushSongToNextItem(this.index);
        }

        private void sheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("sheduleToolStripMenuItem_Click");
            controlForm.Schedule_AddSong(this.Index);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlForm.DB_DeleteSong(this.Index);
        }

        private void SongItemControl_DoubleClick(object sender, EventArgs e)
        {
            controlForm.PushSongToNextItemAndProject(this.Index);
        }

        private void projectStripMenuItem_Click(object sender, EventArgs e)
        {
            controlForm.PushSongToNextItemAndProject(this.index);
        }

        /*
         * General
         */
        private void Logi(string info)
        {
            Debug.WriteLine("SongItemControl: " + info);
        }
    }
}
