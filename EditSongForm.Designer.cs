﻿
namespace ProSoft
{
    partial class EditSongForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.songAuthorlabel = new System.Windows.Forms.Label();
            this.songVersesLabel = new System.Windows.Forms.Label();
            this.SongTitleLabel = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.saveSongButton = new System.Windows.Forms.Button();
            this.songAuthorTextBox = new System.Windows.Forms.TextBox();
            this.songVersesTextBox = new System.Windows.Forms.TextBox();
            this.songTitleTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // songAuthorlabel
            // 
            this.songAuthorlabel.AutoSize = true;
            this.songAuthorlabel.Location = new System.Drawing.Point(3, 397);
            this.songAuthorlabel.Name = "songAuthorlabel";
            this.songAuthorlabel.Size = new System.Drawing.Size(66, 13);
            this.songAuthorlabel.TabIndex = 15;
            this.songAuthorlabel.Text = "Song Author";
            // 
            // songVersesLabel
            // 
            this.songVersesLabel.AutoSize = true;
            this.songVersesLabel.Location = new System.Drawing.Point(3, 42);
            this.songVersesLabel.Name = "songVersesLabel";
            this.songVersesLabel.Size = new System.Drawing.Size(77, 13);
            this.songVersesLabel.TabIndex = 14;
            this.songVersesLabel.Text = "Song Verse(s)*";
            // 
            // SongTitleLabel
            // 
            this.SongTitleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SongTitleLabel.AutoSize = true;
            this.SongTitleLabel.Location = new System.Drawing.Point(3, -2);
            this.SongTitleLabel.Name = "SongTitleLabel";
            this.SongTitleLabel.Size = new System.Drawing.Size(59, 13);
            this.SongTitleLabel.TabIndex = 13;
            this.SongTitleLabel.Text = "Song Title*";
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(6, 439);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 12;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // saveSongButton
            // 
            this.saveSongButton.Location = new System.Drawing.Point(713, 439);
            this.saveSongButton.Name = "saveSongButton";
            this.saveSongButton.Size = new System.Drawing.Size(75, 23);
            this.saveSongButton.TabIndex = 11;
            this.saveSongButton.Text = "Save";
            this.saveSongButton.UseVisualStyleBackColor = true;
            // 
            // songAuthorTextBox
            // 
            this.songAuthorTextBox.Location = new System.Drawing.Point(0, 413);
            this.songAuthorTextBox.Name = "songAuthorTextBox";
            this.songAuthorTextBox.Size = new System.Drawing.Size(800, 20);
            this.songAuthorTextBox.TabIndex = 10;
            // 
            // songVersesTextBox
            // 
            this.songVersesTextBox.Location = new System.Drawing.Point(0, 58);
            this.songVersesTextBox.Multiline = true;
            this.songVersesTextBox.Name = "songVersesTextBox";
            this.songVersesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.songVersesTextBox.Size = new System.Drawing.Size(800, 327);
            this.songVersesTextBox.TabIndex = 9;
            // 
            // songTitleTextBox
            // 
            this.songTitleTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.songTitleTextBox.Location = new System.Drawing.Point(0, 5);
            this.songTitleTextBox.Name = "songTitleTextBox";
            this.songTitleTextBox.Size = new System.Drawing.Size(800, 20);
            this.songTitleTextBox.TabIndex = 8;
            this.songTitleTextBox.Tag = "";
            // 
            // EditSongForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 469);
            this.Controls.Add(this.songAuthorlabel);
            this.Controls.Add(this.songVersesLabel);
            this.Controls.Add(this.SongTitleLabel);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.saveSongButton);
            this.Controls.Add(this.songAuthorTextBox);
            this.Controls.Add(this.songVersesTextBox);
            this.Controls.Add(this.songTitleTextBox);
            this.Name = "EditSongForm";
            this.Text = "EditSongForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label songAuthorlabel;
        private System.Windows.Forms.Label songVersesLabel;
        private System.Windows.Forms.Label SongTitleLabel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button saveSongButton;
        private System.Windows.Forms.TextBox songAuthorTextBox;
        private System.Windows.Forms.TextBox songVersesTextBox;
        private System.Windows.Forms.TextBox songTitleTextBox;
    }
}