﻿
namespace ProSoft
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pickProjectionBodyBackgroundColorButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.projectionBodyFontSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.projectionBodyFontSizeMultpilierNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.spacerPanel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.projectionPanelPaddingTopNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.projectionFontFamilyComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.projectionPanelPaddingBottomNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.projectionPanelPaddingRightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.projectionPanelPaddingLeftNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.projectionPanelFontSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.projectionBodyLabelTextAlignComboBox = new System.Windows.Forms.ComboBox();
            this.spacerPanel = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.clearBiblesButton = new System.Windows.Forms.Button();
            this.bottomSpacerPanel = new System.Windows.Forms.Panel();
            this.projectionPanel = new System.Windows.Forms.Panel();
            this.projectionBodyLabel = new System.Windows.Forms.Label();
            this.projectionHeaderLabel = new System.Windows.Forms.Label();
            this.projectionFooterLabel = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectionBodyFontSizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionBodyFontSizeMultpilierNumericUpDown)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingTopNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingBottomNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingRightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingLeftNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelFontSizeNumericUpDown)).BeginInit();
            this.projectionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(16);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.projectionPanel);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.label6);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel1);
            this.flowLayoutPanel1.Controls.Add(this.spacerPanel1);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.spacerPanel);
            this.flowLayoutPanel1.Controls.Add(this.label16);
            this.flowLayoutPanel1.Controls.Add(this.clearBiblesButton);
            this.flowLayoutPanel1.Controls.Add(this.bottomSpacerPanel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(16, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(8, 8, 8, 48);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(234, 418);
            this.flowLayoutPanel1.TabIndex = 8;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(195, 34);
            this.label6.TabIndex = 2;
            this.label6.Text = "Projection General Settings";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pickProjectionBodyBackgroundColorButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.projectionBodyFontSizeNumericUpDown, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.projectionBodyFontSizeMultpilierNumericUpDown, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 45);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(195, 111);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 26);
            this.label5.TabIndex = 6;
            this.label5.Text = "Text Size Multiplier";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 26);
            this.label4.TabIndex = 4;
            this.label4.Text = "Text Size";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Text Color";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 30);
            this.label2.TabIndex = 0;
            this.label2.Text = "Background Color";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pickProjectionBodyBackgroundColorButton
            // 
            this.pickProjectionBodyBackgroundColorButton.Location = new System.Drawing.Point(100, 3);
            this.pickProjectionBodyBackgroundColorButton.Name = "pickProjectionBodyBackgroundColorButton";
            this.pickProjectionBodyBackgroundColorButton.Size = new System.Drawing.Size(75, 24);
            this.pickProjectionBodyBackgroundColorButton.TabIndex = 1;
            this.pickProjectionBodyBackgroundColorButton.Tag = "projectionBodyBackgroundColor";
            this.pickProjectionBodyBackgroundColorButton.Text = "Pick Color";
            this.pickProjectionBodyBackgroundColorButton.UseVisualStyleBackColor = true;
            this.pickProjectionBodyBackgroundColorButton.Click += new System.EventHandler(this.pickColorButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(100, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Tag = "projectionBodyForeColor";
            this.button1.Text = "Pick Color";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.pickColorButton_Click);
            // 
            // projectionBodyFontSizeNumericUpDown
            // 
            this.projectionBodyFontSizeNumericUpDown.Location = new System.Drawing.Point(100, 62);
            this.projectionBodyFontSizeNumericUpDown.Name = "projectionBodyFontSizeNumericUpDown";
            this.projectionBodyFontSizeNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionBodyFontSizeNumericUpDown.TabIndex = 5;
            this.projectionBodyFontSizeNumericUpDown.Tag = "projectionBodyFontSize";
            this.projectionBodyFontSizeNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // projectionBodyFontSizeMultpilierNumericUpDown
            // 
            this.projectionBodyFontSizeMultpilierNumericUpDown.DecimalPlaces = 1;
            this.projectionBodyFontSizeMultpilierNumericUpDown.Location = new System.Drawing.Point(100, 88);
            this.projectionBodyFontSizeMultpilierNumericUpDown.Name = "projectionBodyFontSizeMultpilierNumericUpDown";
            this.projectionBodyFontSizeMultpilierNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionBodyFontSizeMultpilierNumericUpDown.TabIndex = 7;
            this.projectionBodyFontSizeMultpilierNumericUpDown.Tag = "projectionBodyFontSizeMultpilier";
            this.projectionBodyFontSizeMultpilierNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // spacerPanel1
            // 
            this.spacerPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.spacerPanel1.Location = new System.Drawing.Point(11, 162);
            this.spacerPanel1.Name = "spacerPanel1";
            this.spacerPanel1.Size = new System.Drawing.Size(195, 16);
            this.spacerPanel1.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Projection Body Settings";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label14, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.projectionPanelPaddingTopNumericUpDown, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.projectionFontFamilyComboBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.projectionPanelPaddingBottomNumericUpDown, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.projectionPanelPaddingRightNumericUpDown, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.button2, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.button3, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.projectionPanelPaddingLeftNumericUpDown, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.projectionPanelFontSizeNumericUpDown, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.projectionBodyLabelTextAlignComboBox, 1, 8);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(11, 201);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(195, 242);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 215);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 27);
            this.label9.TabIndex = 23;
            this.label9.Tag = "";
            this.label9.Text = "Text Alignment";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(3, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 26);
            this.label15.TabIndex = 21;
            this.label15.Tag = "";
            this.label15.Text = "Text Size";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 29);
            this.label14.TabIndex = 18;
            this.label14.Tag = "";
            this.label14.Text = "Background Color";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(3, 160);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 29);
            this.label13.TabIndex = 17;
            this.label13.Tag = "";
            this.label13.Text = "Text Color";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(3, 105);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 26);
            this.label12.TabIndex = 16;
            this.label12.Tag = "";
            this.label12.Text = "Padding Left";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 26);
            this.label11.TabIndex = 13;
            this.label11.Tag = "";
            this.label11.Text = "Padding Right";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 26);
            this.label10.TabIndex = 11;
            this.label10.Tag = "";
            this.label10.Text = "Padding Bottom";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectionPanelPaddingTopNumericUpDown
            // 
            this.projectionPanelPaddingTopNumericUpDown.Location = new System.Drawing.Point(100, 30);
            this.projectionPanelPaddingTopNumericUpDown.Name = "projectionPanelPaddingTopNumericUpDown";
            this.projectionPanelPaddingTopNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionPanelPaddingTopNumericUpDown.TabIndex = 10;
            this.projectionPanelPaddingTopNumericUpDown.Tag = "projectionPanelPaddingTop";
            this.projectionPanelPaddingTopNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // projectionFontFamilyComboBox
            // 
            this.projectionFontFamilyComboBox.FormattingEnabled = true;
            this.projectionFontFamilyComboBox.Location = new System.Drawing.Point(100, 3);
            this.projectionFontFamilyComboBox.Name = "projectionFontFamilyComboBox";
            this.projectionFontFamilyComboBox.Size = new System.Drawing.Size(92, 21);
            this.projectionFontFamilyComboBox.TabIndex = 8;
            this.projectionFontFamilyComboBox.Tag = "ProjectionPanelFontFamily";
            this.projectionFontFamilyComboBox.SelectedIndexChanged += new System.EventHandler(this.projectionFontFamilyComboBox_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 27);
            this.label7.TabIndex = 7;
            this.label7.Text = "Font Family";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 26);
            this.label8.TabIndex = 9;
            this.label8.Tag = "";
            this.label8.Text = "Padding Top";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectionPanelPaddingBottomNumericUpDown
            // 
            this.projectionPanelPaddingBottomNumericUpDown.Location = new System.Drawing.Point(100, 56);
            this.projectionPanelPaddingBottomNumericUpDown.Name = "projectionPanelPaddingBottomNumericUpDown";
            this.projectionPanelPaddingBottomNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionPanelPaddingBottomNumericUpDown.TabIndex = 12;
            this.projectionPanelPaddingBottomNumericUpDown.Tag = "projectionPanelPaddingBottom";
            this.projectionPanelPaddingBottomNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // projectionPanelPaddingRightNumericUpDown
            // 
            this.projectionPanelPaddingRightNumericUpDown.Location = new System.Drawing.Point(100, 82);
            this.projectionPanelPaddingRightNumericUpDown.Name = "projectionPanelPaddingRightNumericUpDown";
            this.projectionPanelPaddingRightNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionPanelPaddingRightNumericUpDown.TabIndex = 14;
            this.projectionPanelPaddingRightNumericUpDown.Tag = "projectionPanelPaddingRight";
            this.projectionPanelPaddingRightNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(100, 134);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 19;
            this.button2.Tag = "projectionPanelBackColor";
            this.button2.Text = "Pick Color";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.pickColorButton_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(100, 163);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 20;
            this.button3.Tag = "projectionPanelForeColor";
            this.button3.Text = "Pick Color";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.pickColorButton_Click);
            // 
            // projectionPanelPaddingLeftNumericUpDown
            // 
            this.projectionPanelPaddingLeftNumericUpDown.Location = new System.Drawing.Point(100, 108);
            this.projectionPanelPaddingLeftNumericUpDown.Name = "projectionPanelPaddingLeftNumericUpDown";
            this.projectionPanelPaddingLeftNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionPanelPaddingLeftNumericUpDown.TabIndex = 15;
            this.projectionPanelPaddingLeftNumericUpDown.Tag = "projectionPanelPaddingLeft";
            this.projectionPanelPaddingLeftNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // projectionPanelFontSizeNumericUpDown
            // 
            this.projectionPanelFontSizeNumericUpDown.Location = new System.Drawing.Point(100, 192);
            this.projectionPanelFontSizeNumericUpDown.Name = "projectionPanelFontSizeNumericUpDown";
            this.projectionPanelFontSizeNumericUpDown.Size = new System.Drawing.Size(92, 20);
            this.projectionPanelFontSizeNumericUpDown.TabIndex = 22;
            this.projectionPanelFontSizeNumericUpDown.Tag = "ProjectionPanelFontSize";
            this.projectionPanelFontSizeNumericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // projectionBodyLabelTextAlignmentComboBox
            // 
            this.projectionBodyLabelTextAlignComboBox.FormattingEnabled = true;
            this.projectionBodyLabelTextAlignComboBox.Location = new System.Drawing.Point(100, 218);
            this.projectionBodyLabelTextAlignComboBox.Name = "projectionBodyLabelTextAlignmentComboBox";
            this.projectionBodyLabelTextAlignComboBox.Size = new System.Drawing.Size(92, 21);
            this.projectionBodyLabelTextAlignComboBox.TabIndex = 24;
            this.projectionBodyLabelTextAlignComboBox.Tag = "ProjectionPanelFontFamily";
            this.projectionBodyLabelTextAlignComboBox.SelectedIndexChanged += new System.EventHandler(this.projectionBodyLabelTextAlignmentComboBox_SelectedIndexChanged);
            // 
            // spacerPanel
            // 
            this.spacerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.spacerPanel.Location = new System.Drawing.Point(11, 449);
            this.spacerPanel.Name = "spacerPanel";
            this.spacerPanel.Size = new System.Drawing.Size(195, 16);
            this.spacerPanel.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 468);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(195, 17);
            this.label16.TabIndex = 6;
            this.label16.Text = "Bible Settings";
            // 
            // clearBiblesButton
            // 
            this.clearBiblesButton.Location = new System.Drawing.Point(11, 488);
            this.clearBiblesButton.Name = "clearBiblesButton";
            this.clearBiblesButton.Size = new System.Drawing.Size(75, 23);
            this.clearBiblesButton.TabIndex = 7;
            this.clearBiblesButton.Text = "Clear bibles";
            this.clearBiblesButton.UseVisualStyleBackColor = true;
            this.clearBiblesButton.Click += new System.EventHandler(this.clearBiblesButton_Click);
            // 
            // bottomSpacerPanel
            // 
            this.bottomSpacerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.bottomSpacerPanel.Location = new System.Drawing.Point(11, 517);
            this.bottomSpacerPanel.Name = "bottomSpacerPanel";
            this.bottomSpacerPanel.Size = new System.Drawing.Size(195, 64);
            this.bottomSpacerPanel.TabIndex = 9;
            // 
            // projectionPanel
            // 
            this.projectionPanel.Controls.Add(this.projectionBodyLabel);
            this.projectionPanel.Controls.Add(this.projectionHeaderLabel);
            this.projectionPanel.Controls.Add(this.projectionFooterLabel);
            this.projectionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionPanel.Location = new System.Drawing.Point(0, 0);
            this.projectionPanel.Name = "projectionPanel";
            this.projectionPanel.Size = new System.Drawing.Size(530, 450);
            this.projectionPanel.TabIndex = 0;
            // 
            // projectionBodyLabel
            // 
            this.projectionBodyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionBodyLabel.Location = new System.Drawing.Point(0, 20);
            this.projectionBodyLabel.Name = "projectionBodyLabel";
            this.projectionBodyLabel.Size = new System.Drawing.Size(530, 410);
            this.projectionBodyLabel.TabIndex = 1;
            this.projectionBodyLabel.Text = "Lorem ipsum dolor sit amet, \r\nconsectetur adipiscing elit, \r\nsed do eiusmod tempo" +
    "r incididunt ut labore et dolore magna aliqua.\r\nUt enim ad minim veniam.\r\n";
            this.projectionBodyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // projectionHeaderLabel
            // 
            this.projectionHeaderLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.projectionHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projectionHeaderLabel.Location = new System.Drawing.Point(0, 0);
            this.projectionHeaderLabel.Name = "projectionHeaderLabel";
            this.projectionHeaderLabel.Size = new System.Drawing.Size(530, 20);
            this.projectionHeaderLabel.TabIndex = 0;
            this.projectionHeaderLabel.Text = "label1";
            this.projectionHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // projectionFooterLabel
            // 
            this.projectionFooterLabel.AutoEllipsis = true;
            this.projectionFooterLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.projectionFooterLabel.Location = new System.Drawing.Point(0, 430);
            this.projectionFooterLabel.Name = "projectionFooterLabel";
            this.projectionFooterLabel.Size = new System.Drawing.Size(530, 20);
            this.projectionFooterLabel.TabIndex = 2;
            this.projectionFooterLabel.Text = "label3";
            this.projectionFooterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectionBodyFontSizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionBodyFontSizeMultpilierNumericUpDown)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingTopNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingBottomNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingRightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelPaddingLeftNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectionPanelFontSizeNumericUpDown)).EndInit();
            this.projectionPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel projectionPanel;
        private System.Windows.Forms.Label projectionFooterLabel;
        private System.Windows.Forms.Label projectionBodyLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button pickProjectionBodyBackgroundColorButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown projectionBodyFontSizeNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown projectionBodyFontSizeMultpilierNumericUpDown;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox projectionFontFamilyComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown projectionPanelPaddingTopNumericUpDown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel spacerPanel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown projectionPanelPaddingBottomNumericUpDown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown projectionPanelPaddingLeftNumericUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown projectionPanelPaddingRightNumericUpDown;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown projectionPanelFontSizeNumericUpDown;
        private System.Windows.Forms.Label projectionHeaderLabel;
        private System.Windows.Forms.Button clearBiblesButton;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel spacerPanel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox projectionBodyLabelTextAlignComboBox;
        private System.Windows.Forms.Panel bottomSpacerPanel;
    }
}