﻿
namespace ProSoft
{
    partial class NextItemVersesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NextItemVersesControl));
            this.nextItemVerseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nextItemVerseLabel
            // 
            this.nextItemVerseLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextItemVerseLabel.Location = new System.Drawing.Point(0, 0);
            this.nextItemVerseLabel.MaximumSize = new System.Drawing.Size(100, 0);
            this.nextItemVerseLabel.Name = "nextItemVerseLabel";
            this.nextItemVerseLabel.Size = new System.Drawing.Size(100, 50);
            this.nextItemVerseLabel.TabIndex = 0;
            this.nextItemVerseLabel.Text = resources.GetString("nextItemVerseLabel.Text");
            this.nextItemVerseLabel.DoubleClick += new System.EventHandler(this.nextItemVerseLabel_DoubleClick);
            // 
            // NextItemVersesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.nextItemVerseLabel);
            this.Name = "NextItemVersesControl";
            this.Size = new System.Drawing.Size(100, 50);
            this.Resize += new System.EventHandler(this.NextItemVersesControl_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label nextItemVerseLabel;
    }
}
