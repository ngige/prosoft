﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class ProjectionItemsControl : UserControl
    {
        Color originalBackColor;

        public ProjectionItemsControl()
        {
            InitializeComponent();

            originalBackColor = this.BackColor;
        }

        private ControlForm controlForm;
        private bool isActive;
        private int index;
        private string title;
        private string verse;

        public ControlForm Control
        {
            set { controlForm = value; }
        }

        public bool IsActive
        {
            get { return isActive; }
            set { 
                isActive = value;  
                if(isActive) this.BackColor = SystemColors.ActiveCaption; else this.BackColor = originalBackColor; 
            }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Verse
        {
            get { return verse; }
            set { verse = value; projectionItemControlLabel.Text = verse; }
        }

        private void ProjectionItemControlLabel_Click(object sender, EventArgs e)
        {
            this.IsActive = true;
            controlForm.ProjectSongVerse(index);
        }
    }
}
