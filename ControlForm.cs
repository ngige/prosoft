﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class ControlForm : Form
    {
        delegate void SetTextCallback(string text);

        private ProjectionForm projectionForm;
        private AddSongForm addSongForm;
        private EditSongForm editSongForm;
        private SettingsForm settingsForm;

        private SQLiteConnection sqlite_conn;
        //private SQLiteCommand sqlite_cmd;
        string songDatabasePath;

        List<Song> songsList = new List<Song>();
        public List<SongItemControl> songItemControlList = new List<SongItemControl>();
        List<NextItemVersesControl> nextItemVersesControlList = new List<NextItemVersesControl>();
        List<ProjectionItemsControl> projectionItemsControlLists = new List<ProjectionItemsControl>();
        List<ScheduleItemControl> scheduleItemControlList = new List<ScheduleItemControl>();

        public string activeSongTitle, activeSongVerses, activeSongAuthor = "";

        bool sortSongByTitle = true;
        bool sortSongByTitleASC = false;
        bool sortSongByDate = false;

        /*
         * Bible
         */
        bool bibleLoaded = false;
        Bible activeBible = null;

        int activeBibleBookIndex = 0;
        int activeBibleBookChapterIndex = 0;
        List<int> activeBibleBookChapterVersesIndexList= new List<int>();
        public int activeBibleVerseIndex = -1;

        List<BibleData> bibleDataList = new List<BibleData>();
        List<Bible> bibleList = new List<Bible>();
        // List<BibleVerseControl> bibleVerseControlList = new List<BibleVerseControl>();

        List<string> activeBibleBookNames = new List<string>();

        string searchTextBibleBookName = "";
        string previousSearchedBibleBookName = "";

        string searchTextBibleBookChapter = "";
        string searchTextBibleBookChapterVerse = "";

        /*
         * Public
         */
        public bool isControlKeyPressed = false;

        public ControlForm()
        {
            addSongForm = new AddSongForm(this);
            editSongForm = new EditSongForm(this);
            projectionForm = new ProjectionForm();
            settingsForm = new SettingsForm(this);

            InitializeComponent();
            searchSongTextBox.Text = "search*";

            InitializeDatabase();

            InitiazeBibles();
        }

        /*
         *  Projection Window
         */
        public void ToggleProjectionForm()
         {
            if (projectionForm.Visible)
            {
                projectionForm.Hide();
            } else
            {
                ShowProjectionForm(0);
            }
         }

        public void ShowProjectionForm(int screenNumber)
        {
            Screen[] screens = Screen.AllScreens;

            for(int i = 0; i < screens.Length; i++)
            {
                LogI("ShowProjectionForm: " + i + " - " + screens[i].DeviceName);
            }

            if (screens.Length.Equals(0)) screenNumber = 0;

            Debug.WriteLine("screenNumber: " + screenNumber);

            Screen targetScreen = screens[screenNumber];

            projectionForm.StartPosition = FormStartPosition.Manual;
            projectionForm.Size = new Size(500, 500);

/*            projectionForm.Location = targetScreen.WorkingArea.Location;
            projectionForm.FormBorderStyle = FormBorderStyle.None;
            projectionForm.WindowState = FormWindowState.Maximized;
            projectionForm.Bounds = targetScreen.Bounds;*/

            projectionForm.Show();
        }

        public void GetExternalScreen(int songIndex)
        {
            
        }

        internal void ProjectionFormUpdateUI()
        {
            projectionForm.UpdateUI();
        }

        /*
         *  Song
         */
        public void ShowAddSongForm()
        {
            addSongForm.ShowDialog();
            //addSongForm.Alert("hello");
        }

        public void ShowEditSongForm(int songItemControlListIndex)
        {
            addSongForm.editSong(songItemControlList[songItemControlListIndex], -1);
        }

        public void ShowEditSongForm(int songItemControlListIndex, int scheduleItemControlListIndex)
        {
            addSongForm.editSong(songItemControlList[songItemControlListIndex], scheduleItemControlListIndex);
        }

        /*
         * Song item list
         */
        private int SongItemControlList_AddSong(int id, string title, string verses, string author)
        {
            SongItemControl songItemControl = new SongItemControl();

            songItemControl.Control = this;
            songItemControl.Id = id;
            songItemControl.Index = songItemControlList.Count;
            songItemControl.Title = title;
            songItemControl.Verses = verses;
            songItemControl.Author = author;
            songItemControl.Width = songsFlowLayoutPanel.Width;

            songItemControlList.Add(songItemControl);
            songsFlowLayoutPanel.Controls.Add(songItemControl);

            return songItemControlList.Count - 1;
        }

        private void SearchSong(string searchText)
        {
            LogI("SearchSong searchText: " + searchText);

            List<SongItemControl> songItemControlsSearchResult = songItemControlList.
                Where(songItemControl => Regex.IsMatch(songItemControl.Title, searchText, RegexOptions.IgnoreCase) || Regex.IsMatch(songItemControl.Verses, searchText, RegexOptions.IgnoreCase) || Regex.IsMatch(songItemControl.Author, searchText, RegexOptions.IgnoreCase)).ToList();

            updateSongsFlowLayoutPanel(songItemControlsSearchResult);
        }

        private void SortSong()
        {
            LogI("SortSong, sortSongByTitle: " + sortSongByTitle + ", sortSongByDate:" + sortSongByDate);

            // Getcurrently displayed items
            List<SongItemControl> songItemControlListCurrent = new List<SongItemControl>();
            foreach (SongItemControl songItemControl in songsFlowLayoutPanel.Controls)
            {
                songItemControlListCurrent.Add(songItemControl);
            }

            if (sortSongByTitle) {
                List<SongItemControl> songItemControlsSearchSort = 
                    (sortSongByTitleASC)?songItemControlListCurrent.OrderBy(songItemControl => songItemControl.Title).ToList() : songItemControlListCurrent.OrderByDescending(songItemControl => songItemControl.Title).ToList();
                updateSongsFlowLayoutPanel(songItemControlsSearchSort);
            } else {

            }
        }

        private void updateSongsFlowLayoutPanel(List<SongItemControl> songItemControls)
        {
            songsFlowLayoutPanel.Controls.Clear();
            foreach (SongItemControl songItemControl in songItemControls)
            {
                songsFlowLayoutPanel.Controls.Add(songItemControl);
            }
        }


        /*
         * Next
         */
        internal void PushSongToNextItem(int index)
        {
            nextItemTitleLabel.Text = songItemControlList[index].Title;
            nextItemAuthorLabel.Text = songItemControlList[index].Author;

            nextItemFlowLayoutPanel.Controls.Clear();
            nextItemVersesControlList.Clear();
            string[] verses = songItemControlList[index].Verses.Split(new string[] { "\n\r" }, StringSplitOptions.None);
            int i = 0;
            foreach (var verse in verses)
            {
                NextItemVersesControl nextItemVersesControlItem = new NextItemVersesControl();
                nextItemVersesControlItem.Control = this;
                nextItemVersesControlItem.SongIndex = index;
                nextItemVersesControlItem.Index = i;
                nextItemVersesControlItem.Verse = verse;
                nextItemVersesControlItem.Width = nextItemFlowLayoutPanel.Width;
                nextItemVersesControlItem.Width_ = nextItemFlowLayoutPanel.Width;
                nextItemVersesControlList.Add(nextItemVersesControlItem);
                nextItemFlowLayoutPanel.Controls.Add(nextItemVersesControlItem);
                //nextItemVersesControlItem.Width = nextItemVersesFlowLayoutPanel.Width;
                i++;
            }
        }

        internal void PushSongToNextItemAndProject(int index)
        {
            PushSongToNextItem(index);
            SetSongProjectionItem(index, 0);
        }

        internal void NextItemPanel_AddBibleVerses(Verse verse)
        {
            List<Verse> verses = new List<Verse>();
            verses.Add(verse);

            NextItemBibleVerseControl nextItemBibleVerseControl;
            if (isControlKeyPressed && nextItemFlowLayoutPanel.Controls.Count > 0)
            {
                /*
                 * Cumulate Verses
                 */
                nextItemBibleVerseControl = (NextItemBibleVerseControl) nextItemFlowLayoutPanel.Controls[0];
                nextItemBibleVerseControl.Verse += formatBibleVerse(verse);

                activeBibleBookChapterVersesIndexList.Add(verse.number-1);
            } else
            {
                /*
                 * Unhighlight all verses
                 */
                BibleOneVersesFlowLayoutPanel_UnhighlightVerses();

                nextItemFlowLayoutPanel.Controls.Clear();

                activeBibleBookChapterVersesIndexList.Clear();
                activeBibleBookChapterVersesIndexList.Add(verse.number-1);

                // Create verses item
                nextItemBibleVerseControl = new NextItemBibleVerseControl(
                    nextItemFlowLayoutPanel.Controls.Count, this, formatBibleVerse(verse)
                );
                nextItemBibleVerseControl.Width = nextItemFlowLayoutPanel.Width;
                nextItemFlowLayoutPanel.Controls.Add(nextItemBibleVerseControl);

                //* activeBibleBookChapterVersesIndexList.Clear();
            }
        }

        private string formatBibleVerse(Verse verse)
        {
            return " v." + verse.number + " " + verse.verse;
        }

        /*
         *  Projection
         */
        internal void SetSongProjectionItem(int songIndex, int activeVerseIndex)
        {
            projectionItemFlowLayoutPanel.Controls.Clear();
            projectionItemsControlLists.Clear();
            string[] verses = songItemControlList[songIndex].Verses.Split(new string[] { "\n\r" }, StringSplitOptions.None);

            int i = 0;
            foreach (var verse in verses)
            {
                ProjectionItemsControl projectionItemsControlItem = new ProjectionItemsControl();
                projectionItemsControlItem.Control = this;
                projectionItemsControlItem.Index = i;
                projectionItemsControlItem.Verse = verse;
                projectionItemsControlLists.Add(projectionItemsControlItem);
                if (i == activeVerseIndex)
                {
                    projectionItemsControlItem.IsActive = true;
                    ProjectSongVerse(i);
                }
                projectionItemFlowLayoutPanel.Controls.Add(projectionItemsControlItem);
                projectionItemsControlItem.Width = projectionItemFlowLayoutPanel.Width;
                i++;
            }

            // Scroll to Active Item
            var activeControl = projectionItemFlowLayoutPanel.Controls[activeVerseIndex];
            Point point = activeControl.Location - new Size(projectionItemFlowLayoutPanel.AutoScrollPosition);
            point = point - new Size(activeControl.Margin.Left, activeControl.Margin.Top);
            projectionItemFlowLayoutPanel.AutoScrollPosition = point;
        }


        internal void ProjectSongVerse(int index)
        {
            // Display in Projector
            projectionForm.Project(projectionItemsControlLists[index].Verse);

            // Make Others Inactive
            foreach(ProjectionItemsControl projectionItemsControl in projectionItemsControlLists)
            {
                if (projectionItemsControl.Index == index) continue;
                projectionItemsControl.IsActive = false;
            }
        }


        internal void ProjectionPanel_AddBibleVerses(int index)
        {
            projectionItemFlowLayoutPanel.Controls.Clear();

            NextItemBibleVerseControl nextItemBibleVerseControl = (NextItemBibleVerseControl)nextItemFlowLayoutPanel.Controls[index];

            // Create verses item
            ProjectionItemBibleVerseControl projectionItemBibleVerseControl = new ProjectionItemBibleVerseControl(
                nextItemFlowLayoutPanel.Controls.Count, this, nextItemBibleVerseControl.verse
            );

            projectionItemBibleVerseControl.Width = nextItemFlowLayoutPanel.Width;
            projectionItemFlowLayoutPanel.Controls.Add(projectionItemBibleVerseControl);

            // Project window
            Project(nextItemBibleVerseControl.verse);
        }

        internal void ExpressProjectbibleVerses()
        {

            LogI("activeBibleBookIndex: " + activeBibleBookIndex);
            LogI("activeBibleBookChapterIndex: " + activeBibleBookChapterIndex);

            for (int i = 0; i < activeBibleBookChapterVersesIndexList.Count; i++)
            {
                LogI("activeBibleBookChapterVersesIndexList: " + activeBibleBookChapterVersesIndexList[i]);
            }
            
            /*
             * Clear the projection panel
             */
            projectionItemFlowLayoutPanel.Controls.Clear();

            string versesToProject = "";

            // Assemble the verses
            Chapter chapter = activeBible.books[activeBibleBookIndex].chapters[activeBibleBookChapterIndex];
            foreach (int verseIndex in activeBibleBookChapterVersesIndexList)
            {
                versesToProject += formatBibleVerse(chapter.verses[verseIndex]);
            }

            ProjectionItemBibleVerseControl projectionItemBibleVerseControl = new ProjectionItemBibleVerseControl(
                nextItemFlowLayoutPanel.Controls.Count, this, versesToProject
            );

            projectionItemBibleVerseControl.Width = nextItemFlowLayoutPanel.Width;
            projectionItemFlowLayoutPanel.Controls.Add(projectionItemBibleVerseControl);
        }

        /*
         * Projection window
         */
        internal void Project(string verse)
        {
            projectionForm.Project(verse);
        }

        /*
         * Schedule
         */
        internal void Schedule_AddSong(int index)
        {
           LogI("ScheduleAddSong index: " + index);

            SongItemControl songListItem = songItemControlList[index];

            ScheduleItemControl scheduleItemControl = new ScheduleItemControl();
            scheduleItemControl.ControlForm_ = this;
            scheduleItemControl.Id = songListItem.Id;
            scheduleItemControl.Index = scheduleItemControlList.Count;
            scheduleItemControl.SongControlListindex = index;
            scheduleItemControl.Title = songListItem.Title;
            scheduleItemControl.Verses = songListItem.Verses;
            scheduleItemControl.Author = songListItem.Author;
            scheduleItemControl.Width = sheduleFlowLayoutPanel.Width;

            sheduleFlowLayoutPanel.Controls.Add(scheduleItemControl);
        }

        internal void ScheduleRemoveItem(int index)
        {
            sheduleFlowLayoutPanel.Controls.Remove(sheduleFlowLayoutPanel.Controls[index]);
        }

        private void SaveSchedule()
        {
            if (sheduleFlowLayoutPanel.Controls.Count == 0) 
            {
                ShowAlert("Cannot save schedule", "Schedule is empty, add some songs to the shedule first!");
                return;
            }

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = DateTime.Now.ToString("dd-MM-yyyy");
            saveFileDialog1.Filter = "Json files (*.json)|*.json";
            saveFileDialog1.Title = "Save Shedule";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                LogI("SaveSchedule saveFileDialog1.FileName: " + saveFileDialog1.FileName);

                // Create Songs Array
                List<Song> songList = new List<Song>();

                foreach (ScheduleItemControl scheduleItemControl in sheduleFlowLayoutPanel.Controls)
                {
                    Song song = new Song(
                        scheduleItemControl.Title,
                        scheduleItemControl.Verses,
                        scheduleItemControl.Author
                    );

                    songList.Add(song);
                }

                string serializedResult = JsonSerializer.Serialize(songList);

                File.WriteAllText(saveFileDialog1.FileName, serializedResult);
            }
        }

        private void OpenShedule()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            // openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "Json files (*.json)|*.json";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {

                try
                {
                    string scheduleString = File.ReadAllText(openFileDialog.FileName);
                    List<Song> importedSheduleSongs = JsonSerializer.Deserialize<List<Song>>(scheduleString);

                    foreach (Song song in importedSheduleSongs)
                    {
                        //  LogI("OpenShedule importedSheduleSong: " + importedSheduleSongs);
                        Schedule_AddSongTo_DB_and_songItemControlList(song);
                    }
                }
                catch (Exception ex)
                {
                    ShowAlert("Opening schedule error: ",
                        "Json format is invalid for file: " + openFileDialog.FileName + ", error:" + ex.Message);
                }
            }
        }

        private void Schedule_AddSongTo_DB_and_songItemControlList(Song song)
        {
            // Check if song is in the Main list by Title and Verses
            List<SongItemControl> songItemControlsSearchResult = songItemControlList.
                Where(songItemControl => songItemControl.Title.Contains(song.Title) || songItemControl.Verses.Contains(song.Verses)).ToList();

            // If song does not exist add it
            int songItemControlListIndex = (songItemControlsSearchResult.Count == 0) ? DB_InsertSong(song) : songItemControlsSearchResult[0].Index; 

            Schedule_AddSong(songItemControlListIndex);
        }

        /*
         * Database
         */
        private void InitializeDatabase()
        {
            string songDatabaseDirectory = @"" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ProSoft";
            string songDatabasePath = songDatabaseDirectory + "\\Songs.db";

            Debug.WriteLine("Database path: " + songDatabasePath);

            // Create Database
            Directory.CreateDirectory(songDatabaseDirectory);

            if (!File.Exists(songDatabasePath))
            {
                using (FileStream fs = File.Create(songDatabasePath)) { }
            }

            songDatabasePath = @"URI=file:" + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ProSoft\\Songs.db";

            Debug.WriteLine("Database path: " + songDatabasePath);

            sqlite_conn = new SQLiteConnection(songDatabasePath);

            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ControlForm InitializeDatabase SQLiteConnection error: " + ex.Message);
                return;
            }

            // Create Songs Table
            SQLiteCommand sqlite_cmd = new SQLiteCommand(sqlite_conn);


            sqlite_cmd.CommandText = @"CREATE TABLE IF NOT EXISTS songs(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                title TEXT, 
                verses TEXT, 
                author TEXT,
                updated TEXT,
                created TEXT
             )";

            int sql_data = sqlite_cmd.ExecuteNonQuery();

            Debug.WriteLine("Create Songs Table: " + sql_data);

            DBGetSongs();
        }

        public int DB_InsertSong(Song song)
        {
            int return_data = -1;

            // Check if song exist
            List<SongItemControl> songItemControlsSearchResult = songItemControlList.
                Where(songItemControl => songItemControl.Title.Contains(song.Title) || songItemControl.Verses.Contains(song.Verses)).ToList();

            song.Title += (songItemControlsSearchResult.Count > 0)? " v" + Convert.ToString(songItemControlsSearchResult.Count) : "" ;

            SQLiteCommand sqlite_cmd = new SQLiteCommand(sqlite_conn);
            sqlite_cmd.CommandText = "INSERT INTO songs(title, verses, author, created) VALUES($title, $verses, $author, $created)";
            sqlite_cmd.Parameters.AddWithValue("title", song.Title);
            sqlite_cmd.Parameters.AddWithValue("verses", song.Verses);
            sqlite_cmd.Parameters.AddWithValue("author", song.Author);
            sqlite_cmd.Parameters.AddWithValue("created", DateTime.Now.ToString("dd/MMM/yyyy"));

            int sql_data = sqlite_cmd.ExecuteNonQuery();

            if (sql_data > -1)
            {
                return_data = SongItemControlList_AddSong((int)sqlite_conn.LastInsertRowId, song.Title, song.Verses, song.Author);
            }

            return return_data;
        }

        public void DBGetSongs()
        {
            SQLiteCommand sqlite_cmd = new SQLiteCommand(sqlite_conn);

            sqlite_cmd.CommandText = "SELECT * FROM songs LIMIT 50";

            SQLiteDataReader sqliteDataReader = sqlite_cmd.ExecuteReader();

            while (sqliteDataReader.Read())
            {
                SongItemControlList_AddSong(
                    sqliteDataReader.GetInt32(0),
                    sqliteDataReader.GetString(1).ToString(),
                    sqliteDataReader.GetString(2).ToString(),
                    sqliteDataReader.GetString(3).ToString());
            }
        }

        internal void DB_UpdateSong(int songItemControlIndex, int scheduleItemControlIndex, string title, string verses, string author)
        {
            LogI("DB_UpdateSong songItemControlIndex: " + songItemControlIndex);
            LogI("DB_UpdateSong scheduleItemControlIndex: " + scheduleItemControlIndex);

            SQLiteCommand sqlite_cmd = new SQLiteCommand(sqlite_conn);
            sqlite_cmd.CommandText = "UPDATE songs SET title=$title, verses=$verses, author=$author WHERE id = $id";
            sqlite_cmd.Parameters.AddWithValue("title", title);
            sqlite_cmd.Parameters.AddWithValue("verses", verses);
            sqlite_cmd.Parameters.AddWithValue("author", author);
            sqlite_cmd.Parameters.AddWithValue("id", songItemControlList[songItemControlIndex].Id);
            int sql_data = sqlite_cmd.ExecuteNonQuery();

            if (sql_data > -1)
            {
                songItemControlList[songItemControlIndex].Title = title;
                songItemControlList[songItemControlIndex].Verses = verses;
                songItemControlList[songItemControlIndex].Author = author;

                if(scheduleItemControlIndex > -1)
                {
                    ScheduleItemControl scheduleItemControl = (ScheduleItemControl)sheduleFlowLayoutPanel.Controls[scheduleItemControlIndex];

                    scheduleItemControl.Title = title;
                    scheduleItemControl.Verses = verses;
                    scheduleItemControl.Author = author;
                }

                addSongForm.SongSaved(true);
            }
            else
            {
                addSongForm.SongSaved(false);
            }
        }

        internal void DB_DeleteSong(int songItemControlIndex)
        {
            SongItemControl songItemControl = (SongItemControl)songsFlowLayoutPanel.Controls[songItemControlIndex];

            SQLiteCommand sqlite_cmd = new SQLiteCommand(sqlite_conn);
            sqlite_cmd.CommandText = "DELETE FROM songs WHERE id = $id";
            sqlite_cmd.Parameters.AddWithValue("id", songItemControl.Id);
            int sql_data = sqlite_cmd.ExecuteNonQuery();

            if (sql_data > -1)
            {
                ShowAlert("Song delete", "DB_DeleteSong Successful");
                songsFlowLayoutPanel.Controls.Remove(songItemControl);
            }
            else ShowAlert("Song delete", "DB_DeleteSong Unsuccessful");
        }

        /*
         *  Bible
         */
        private void InitiazeBibles()
        {
            Bible_GetData();
        }

        private void Bible_Add()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "Json files (*.json)|*.json";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string bibleJSON = File.ReadAllText(openFileDialog.FileName);

                try
                {
                    Bible bible = JsonSerializer.Deserialize<Bible>(bibleJSON);

                    LogI("scheduleString: bible.version: " + bible.version);

                    if (bible.version == null) ShowAlert("Adding Bible Error", "Could not convert JSON. The bible json format is not correct!");
                    else
                    {
                        BibleData bibleData = new BibleData(
                            bible.version,
                            openFileDialog.FileName
                        );

                        bibleDataList.Add(bibleData);

                        Properties.Settings.Default.BibleData = JsonSerializer.Serialize(bibleDataList);
                        Properties.Settings.Default.Save();

                        ShowAlert("Bible imported", bible.version + ", was imported.");

                        Bible_GetData();
                    }
                }
                catch (Exception ex)
                {
                    ShowAlert("Bible import failed",
                        "Bible to be imported is not in the correct format: , error:" + ex.Message);
                }
            }
        }

        private void Bible_GetData()
        {
            string biblesDataString = Properties.Settings.Default.BibleData;

            if (!string.IsNullOrEmpty(biblesDataString))
            {
                try
                {
                    bibleDataList.Clear();
                    bibleDataList = JsonSerializer.Deserialize<List<BibleData>>(Properties.Settings.Default.BibleData);

                    LogI("Bible_GetData bibleDataList: " + bibleDataList.Count);

                    Bible_Load();
                    Bible_ActiveGetBooksNames();
                }
                catch (Exception ex)
                {
                    ShowAlert("Bible error: saved bible data corrupt",
                        "Saved data for added bibles is not in the correct format: , error:" + ex.Message);
                }
            }
        }

        private void Bible_Load()
        {
            bibleList.Clear();

            foreach (BibleData bibleData in bibleDataList)
            {
                try
                {
                    string biblesJSON = File.ReadAllText(bibleData.location);
                    Bible bible = JsonSerializer.Deserialize<Bible>(biblesJSON);

                    if (bible.version == null)
                        ShowAlert("Bible Format Error: " + bibleData.version, "Could not convert JSON. The bible json format is not correct");
                    else
                    {
                        bibleList.Add(bible);
                        bibleLoaded = true;
                    }
                }
                catch (Exception ex)
                {
                    ShowAlert("Bible Location Error: " + bibleData.version,
                        "Could not find bible in the location: " + bibleData.location + ", error:" + ex.Message);
                }

                break;
            }

            /*
             * Set first bible as active
             */
            if (bibleList.Count > 0)
            {
                activeBible = bibleList[0];
                Bible_LoadBook(0, 0);
            }
        }

        private void Bible_ActiveGetBooksNames()
        {
/*            if (activeBibleIndex < bibleList.Count) {
                Bible bible = bibleList[activeBibleIndex];

                foreach (Book book in bible.books)
                {
                    activeBibleBookNames.Add(book.name.ToLower());

                    LogI("------>" + book.name.ToLower());
                }
            }*/
        }

        private void Bible_LoadBook(int bookIndex, int chapterIndex)
        {
            if (activeBible == null) return;

            bibleOneVerseFlowLayoutPanel.Controls.Clear();

            List<Verse> verses = activeBible.books[bookIndex].chapters[chapterIndex].verses;


            bibleOneVerseFlowLayoutPanel.SuspendLayout();
            foreach (Verse verse in verses)
            {
                LogI("Bible_LoadBook bibleOneVerseFlowLayoutPanel.Width: " + bibleOneVerseFlowLayoutPanel.Width);

                BibleVerseControl bibleVerseControl = new BibleVerseControl(this, verse);
                bibleVerseControl.Width = bibleOneVerseFlowLayoutPanel.Width;
                bibleOneVerseFlowLayoutPanel.Controls.Add(bibleVerseControl);
            }
            bibleOneVerseFlowLayoutPanel.ResumeLayout();
        }

        /*
         * Unhighlight all verses
         */
        private void BibleOneVersesFlowLayoutPanel_UnhighlightVerses()
        {
            foreach (BibleVerseControl bibleVerseControl in bibleOneVerseFlowLayoutPanel.Controls)
            {
                bibleVerseControl.Deactivate();
            }
        }


        /*
         * General
         */
        internal void ShowAlert(string title, string message)
        {
            LogI("ShowAlert: " + title + ", " + message);

            alertHeaderLabel.Text = title;
            alertBodyLabel.Text = message;
            alertPanel.Visible = true;

            Task.Delay(3000).ContinueWith(t => CloseAlert());
        }

        private void SetText(string text)
        {
            alertPanel.Visible = false;
            alertHeaderLabel.Text = "";
            alertBodyLabel.Text = "";
        }

        private void CloseAlert()
        {
            if (alertPanel.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] { " (Invoke)" });
            }
            else
            {
                // It's on the same thread, no need for Invoke
                alertPanel.Visible = false;
                alertHeaderLabel.Text = "";
                alertBodyLabel.Text = "";
            }
        }

        public void LogI(string message)
        {
            Debug.WriteLine("ControlForm " + message);
        }

        private void songsFlowLayoutPanel_Resize(object sender, EventArgs e)
        {
            /*
             * Resize flow items sizes
             */
            for(int i = 0; i < songsFlowLayoutPanel.Controls.Count; i++)
            {
                songsFlowLayoutPanel.Controls[i].Width = songsFlowLayoutPanel.Width;
            }

            for(int i = 0; i < nextItemFlowLayoutPanel.Controls.Count; i++)
            {
                nextItemFlowLayoutPanel.Controls[i].Width = nextItemFlowLayoutPanel.Width;
            }

            bibleOneVerseFlowLayoutPanel.SuspendLayout();
            for (int i = 0; i < bibleOneVerseFlowLayoutPanel.Controls.Count; i++)
            {
                bibleOneVerseFlowLayoutPanel.Controls[i].Width = bibleOneVerseFlowLayoutPanel.Width;
            }
            bibleOneVerseFlowLayoutPanel.ResumeLayout();

            //Bible_LoadBook(0,0);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        /*
         * Events
         */
        private void GoLiveButton_Click(object sender, EventArgs e) { ToggleProjectionForm(); }

        private void SaveScheduleButton_Click(object sender, EventArgs e)
        {
            SaveSchedule();
        }

        private void SearchSongTextBox_Leave(object sender, EventArgs e)
        {
            if (searchSongTextBox.Text == "") searchSongTextBox.Text = "search*";
        }

        private void searchSongTextBox_TextChanged(object sender, EventArgs e)
        {
            SearchSong(searchSongTextBox.Text);
        }

        private void SearchSongTextBox_Enter(object sender, EventArgs e)
        {
            if (searchSongTextBox.Text == "search*") searchSongTextBox.Text = "";
        }

        private void sortSongByTitleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sortSongByTitle = true;
            sortSongByDate = false;

            sortSongByTitleASC = !sortSongByTitleASC;

            SortSong();
        }

        private void AddSongButton_Click(object sender, EventArgs e) { ShowAddSongForm();}

        private void openScheduleButton_Click(object sender, EventArgs e)
        {
            OpenShedule();
        }

        private void alertCloseButton_Click(object sender, EventArgs e)
        {
            CloseAlert();
        }

        private void tabControl2_Click(object sender, EventArgs e)
        {
            LogI("+");
        }

        private void AddBibleButton_Click(object sender, EventArgs e)
        {
            Bible_Add();
        }

        /*
         * Keys functions
         */
        private void ControlForm_KeyDown(object sender, KeyEventArgs e)
        {
            LogI("key down" + e.KeyCode);

            if (e.KeyCode == Keys.ControlKey)
            {
                isControlKeyPressed = true;
            }

            /*
             * Enter key, return key
             */
            if (e.KeyCode == Keys.Return)
            {
                ExpressProjectbibleVerses();
            }

            /*
             * Arrow keys
             */
            if (e.KeyCode == Keys.Down)
            {
                // Move down in bible verse list
                if(activeBibleVerseIndex < bibleOneVerseFlowLayoutPanel.Controls.Count-1)
                {
                    // Unhighlight previous
                    if(activeBibleVerseIndex > -1)
                    {
                        BibleVerseControl previousBibleVerseControl = (BibleVerseControl)bibleOneVerseFlowLayoutPanel.Controls[activeBibleVerseIndex];
                        previousBibleVerseControl.Deactivate();
                    }

                    activeBibleVerseIndex++;

                    // Highlight next
                    BibleVerseControl nextBibleVerseControl = (BibleVerseControl) bibleOneVerseFlowLayoutPanel.Controls[activeBibleVerseIndex];
                    nextBibleVerseControl.Activate();

                    // Scroll to active verse
                    Point point = nextBibleVerseControl.Location - new Size(bibleOneVerseFlowLayoutPanel.AutoScrollPosition);
                    point = point - new Size(nextBibleVerseControl.Margin.Left, nextBibleVerseControl.Margin.Top + (bibleOneVerseFlowLayoutPanel.Height - nextBibleVerseControl.Height - 32));
                    bibleOneVerseFlowLayoutPanel.AutoScrollPosition = point;
                }
            }
        }

        private void ControlForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    e.IsInputKey = true;
                    break;
                case Keys.Up:
                    e.IsInputKey = true;
                    break;
            }
        }

        // Ctrl key press
        private void ControlForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                isControlKeyPressed = false;
            }
            if (e.KeyCode == Keys.Enter)
            {
                LogI("Enter button pressed");
            }
        }

        private void bibleSearchBookNameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                bibleSearchBookTextBox.Text = "";
                searchTextBibleBookName = "";
                previousSearchedBibleBookName = "";

            } else if (e.KeyCode == Keys.Right)
            {
                bibleSearchBookChapterTextBox.Focus();
            } else
            {
                activeBibleBookChapterIndex = 0;
                activeBibleBookChapterVersesIndexList.Clear();

                string keyEntered = getKeyValue(e.KeyCode.ToString());

                string previousSearchText = searchTextBibleBookName;
                string searchText = searchTextBibleBookName + keyEntered;

                List<string> searchedBibleBookNames = activeBibleBookNames.Where(x => x.StartsWith(searchText)).ToList();
                List<Book> searchedBibleBookList = activeBible.books.Where(
                    book => book.name.StartsWith(searchText, StringComparison.InvariantCultureIgnoreCase)).ToList();

                if (searchedBibleBookList.Count > 0)
                {
                    // Get first book match
                    activeBibleBookIndex = searchedBibleBookList[0].number - 1;
                    previousSearchedBibleBookName = searchedBibleBookList[0].name;
                    bibleSearchBookTextBox.Text = previousSearchedBibleBookName;

                    // Serach text
                    searchTextBibleBookName = searchText;
                    bibleSearchBookTextBox.Select(0, searchText.Length);
                    bibleSearchBookTextBox.Focus();
                    bibleSearchBookTextBox.ScrollToCaret();
                }
                else
                {
                    // Overwrite Book name
                    bibleSearchBookTextBox.Text = searchTextBibleBookName;

                    // Overwrite text
                    bibleSearchBookTextBox.Select(0, previousSearchText.Length);
                    bibleSearchBookTextBox.Focus();
                    bibleSearchBookTextBox.ScrollToCaret();
                }
            }
        }


        private void bibleSearchBookChapterTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left) bibleSearchBookTextBox.Focus(); 
            else if(e.KeyCode == Keys.Right) bibleSearchChapterVerseTextBox.Focus();
            else if (e.KeyCode == Keys.Back)
            {
                bibleSearchBookChapterTextBox.Text = "";
                searchTextBibleBookChapter = "";
            } else
            {
                activeBibleBookChapterVersesIndexList.Clear();

                char keyValue = getKeyValueChar(e.KeyCode.ToString());

                if (!keyValue.Equals('$'))
                {
                    string searchText = searchTextBibleBookChapter + keyValue;

                    List<Chapter> searchedChapterList = activeBible.books[activeBibleBookIndex].chapters.Where(
                        x => x.number == Convert.ToInt32(searchText)).ToList();

                    if (searchedChapterList.Count > 0)
                    {
                        activeBibleBookChapterIndex = searchedChapterList[0].number;

                        bibleSearchBookChapterTextBox.Text = Convert.ToString(activeBibleBookChapterIndex);
                        activeBibleBookChapterIndex--;

                        searchTextBibleBookChapter = searchText;

                        // Cursor to end of search text
                        bibleSearchBookChapterTextBox.Select(0, searchText.Length);
                        bibleSearchBookChapterTextBox.Focus();
                        bibleSearchBookChapterTextBox.ScrollToCaret();

                        Bible_LoadBook(activeBibleBookIndex, activeBibleBookChapterIndex);
                    }
                    else
                    {
                        bibleSearchBookChapterTextBox.Text = searchTextBibleBookChapter;

                        // Cursor to end of search text
                        bibleSearchBookChapterTextBox.Select(0, searchTextBibleBookChapter.Length);
                        bibleSearchBookChapterTextBox.Focus();
                        bibleSearchBookChapterTextBox.ScrollToCaret();
                    }
                }
            }
        }

        private void bibleSearchChapterVerseTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left) bibleSearchBookChapterTextBox.Focus();
            else if (e.KeyCode == Keys.Back)
            {
                activeBibleBookChapterVersesIndexList.Clear();

                searchTextBibleBookChapterVerse = "";
                bibleSearchChapterVerseTextBox.Text = "";

                // Clear previous active verses
                bibleOneVerseFlowLayoutPanel.SuspendLayout();
                foreach (BibleVerseControl bvc in bibleOneVerseFlowLayoutPanel.Controls)
                {
                    bvc.Deactivate();
                }
                bibleOneVerseFlowLayoutPanel.ResumeLayout();
            }
            else
            {
                char keyValue = getKeyValueChar(e.KeyCode.ToString());

                if (keyValue.Equals('$'))
                {
                    bibleSearchChapterVerseTextBox.Text = searchTextBibleBookChapterVerse;

                    // Highlight and move to end of search text
                    bibleSearchChapterVerseTextBox.Select(0, searchTextBibleBookChapterVerse.Length);
                    bibleSearchChapterVerseTextBox.Focus();
                    bibleSearchChapterVerseTextBox.ScrollToCaret();
                }
                else
                {
                    int firstVerseIndex = -1;
                    int secondVerseIndex = -1;

                    string searchText = searchTextBibleBookChapterVerse + keyValue;
                    string inputText = searchText;

                    // Multiple verses
                    List<string> versesList = searchText.Split('-').ToList();

                    // First verse
                    searchText = versesList[0];
                    List<Verse> searchVerseList = activeBible.books[activeBibleBookIndex].chapters[activeBibleBookChapterIndex].verses.Where(
                            x => x.number == Convert.ToInt32(searchText)).ToList();

                    if (searchVerseList.Count > 0)
                    {
                        // Clear previous active verses
                        bibleOneVerseFlowLayoutPanel.SuspendLayout();
                        foreach (BibleVerseControl bvc in bibleOneVerseFlowLayoutPanel.Controls)
                        {
                            bvc.Deactivate();
                        }
                        bibleOneVerseFlowLayoutPanel.ResumeLayout();

                        firstVerseIndex = searchVerseList[0].number - 1;
                        activeBibleBookChapterVersesIndexList.Add(firstVerseIndex);

                        searchTextBibleBookChapterVerse = inputText;
                        bibleSearchChapterVerseTextBox.Text = inputText;

                        // Highlight and move to end of search text
                        bibleSearchChapterVerseTextBox.Select(0, inputText.Length);
                        bibleSearchChapterVerseTextBox.Focus();
                        bibleSearchChapterVerseTextBox.ScrollToCaret();

                        // Scroll to active verse
                        var activeControl = bibleOneVerseFlowLayoutPanel.Controls[searchVerseList[0].number - 1];
                        Point point = activeControl.Location - new Size(bibleOneVerseFlowLayoutPanel.AutoScrollPosition);
                        point = point - new Size(activeControl.Margin.Left, activeControl.Margin.Top);
                        bibleOneVerseFlowLayoutPanel.AutoScrollPosition = point;

                        // Highlight active verse
                        BibleVerseControl bibleVerseControl = (BibleVerseControl)bibleOneVerseFlowLayoutPanel.Controls[firstVerseIndex];
                        bibleVerseControl.Activate();

                        // Second verse
                        if (versesList.Count() > 1 && !string.IsNullOrEmpty(versesList[1]))
                        {
                            int firstVerse = Convert.ToInt32(versesList[0]);
                            int secondVerse = Convert.ToInt32(versesList[1]);

                            // Second verse should be more
                            if (secondVerse > firstVerse)
                            {
                                searchText = versesList[1];

                                searchVerseList = activeBible.books[activeBibleBookIndex].chapters[activeBibleBookChapterIndex].verses.Where(
                                    x => x.number == Convert.ToInt32(searchText)).ToList();

                                if (searchVerseList.Count > 0)
                                {
                                    secondVerseIndex = searchVerseList[0].number - 1;

                                    // Highlight active verse
                                    bibleVerseControl = (BibleVerseControl)bibleOneVerseFlowLayoutPanel.Controls[secondVerseIndex];
                                    bibleVerseControl.Activate();

                                    // Highlight in between verses
                                    for(int i = firstVerseIndex + 1; i < secondVerseIndex; i++)
                                    {
                                        activeBibleBookChapterVersesIndexList.Add(i);
                                        bibleVerseControl = (BibleVerseControl)bibleOneVerseFlowLayoutPanel.Controls[i];
                                        bibleVerseControl.Activate();
                                    }
                                }

                                activeBibleBookChapterVersesIndexList.Add(secondVerseIndex);
                            }
                        }
                    }
                    else
                    {
                        bibleSearchChapterVerseTextBox.Text = searchTextBibleBookChapterVerse;

                        // Highlight and move to end of search text
                        bibleSearchChapterVerseTextBox.Select(0, searchTextBibleBookChapterVerse.Length);
                        bibleSearchChapterVerseTextBox.Focus();
                        bibleSearchChapterVerseTextBox.ScrollToCaret();

                        searchTextBibleBookChapterVerse = "";
                    }
                }
            }
        }

        private void bibleSearchBookChapterTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void bibleSearchBookTextBox_Enter(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(searchTextBibleBookChapter))
            {
                bibleSearchBookChapterTextBox.Select(0, searchTextBibleBookChapter.Length);
                bibleSearchBookChapterTextBox.Focus();
                bibleSearchBookChapterTextBox.ScrollToCaret();
            }
        }

        private void sortSongContextMenuStripLabel_Click(object sender, EventArgs e)
        {
            sortSongContextMenuStrip.Show(sortSongContextMenuStripLabel, new Point(0, 0));
        }

        /*
         * Main Menu Events
         */
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingsForm.ShowDialog();
        }

        private void ControlForm_Load(object sender, EventArgs e)
        {

        }

        private string getKeyValue(string keyValue)
        {
            if (keyValue.Equals(Keys.NumPad0.ToString())) keyValue = "0";
            if (keyValue.Equals(Keys.NumPad1.ToString())) keyValue = "1";
            if (keyValue.Equals(Keys.NumPad2.ToString())) keyValue = "2";
            if (keyValue.Equals(Keys.NumPad3.ToString())) keyValue = "3";
            if (keyValue.Equals(Keys.NumPad4.ToString())) keyValue = "4";
            if (keyValue.Equals(Keys.NumPad5.ToString())) keyValue = "5";
            if (keyValue.Equals(Keys.NumPad6.ToString())) keyValue = "6";
            if (keyValue.Equals(Keys.NumPad7.ToString())) keyValue = "7";
            if (keyValue.Equals(Keys.NumPad8.ToString())) keyValue = "8";
            if (keyValue.Equals(Keys.NumPad9.ToString())) keyValue = "9";

            if (keyValue.Equals(Keys.D0.ToString())) keyValue = "0";
            if (keyValue.Equals(Keys.D1.ToString())) keyValue = "1";
            if (keyValue.Equals(Keys.D2.ToString())) keyValue = "2";
            if (keyValue.Equals(Keys.D3.ToString())) keyValue = "3";
            if (keyValue.Equals(Keys.D4.ToString())) keyValue = "4";
            if (keyValue.Equals(Keys.D5.ToString())) keyValue = "5";
            if (keyValue.Equals(Keys.D6.ToString())) keyValue = "6";
            if (keyValue.Equals(Keys.D7.ToString())) keyValue = "7";
            if (keyValue.Equals(Keys.D8.ToString())) keyValue = "8";
            if (keyValue.Equals(Keys.D9.ToString())) keyValue = "9";

            if (keyValue.Equals(Keys.Space.ToString())) keyValue = " ";
            if (keyValue.Equals(Keys.OemMinus.ToString())) keyValue = "-";

            return keyValue.ToLower();
        }

        private char getKeyValueChar(string keyName)
        {
            char return_value = '$';

            if (keyName.Equals(Keys.NumPad0.ToString())) return_value = '0';
            if (keyName.Equals(Keys.NumPad1.ToString())) return_value = '1';
            if (keyName.Equals(Keys.NumPad2.ToString())) return_value = '2';
            if (keyName.Equals(Keys.NumPad3.ToString())) return_value = '3';
            if (keyName.Equals(Keys.NumPad4.ToString())) return_value = '4';
            if (keyName.Equals(Keys.NumPad5.ToString())) return_value = '5';
            if (keyName.Equals(Keys.NumPad6.ToString())) return_value = '6';
            if (keyName.Equals(Keys.NumPad7.ToString())) return_value = '7';
            if (keyName.Equals(Keys.NumPad8.ToString())) return_value = '8';
            if (keyName.Equals(Keys.NumPad9.ToString())) return_value = '9';

            if (keyName.Equals(Keys.D0.ToString())) return_value = '0';
            if (keyName.Equals(Keys.D1.ToString())) return_value = '1';
            if (keyName.Equals(Keys.D2.ToString())) return_value = '2';
            if (keyName.Equals(Keys.D3.ToString())) return_value = '3';
            if (keyName.Equals(Keys.D4.ToString())) return_value = '4';
            if (keyName.Equals(Keys.D5.ToString())) return_value = '5';
            if (keyName.Equals(Keys.D6.ToString())) return_value = '6';
            if (keyName.Equals(Keys.D7.ToString())) return_value = '7';
            if (keyName.Equals(Keys.D8.ToString())) return_value = '8';
            if (keyName.Equals(Keys.D9.ToString())) return_value = '9';

            if (keyName.Equals(Keys.OemMinus.ToString())) return_value = '-';

            return return_value;
        }
    }
}
