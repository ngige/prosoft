﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class NextItemVersesControl : UserControl
    {
        public NextItemVersesControl()
        {
            InitializeComponent();
        }

        private ControlForm controlForm;
        private int songIndex;
        private int index;
        private string title;
        private string verse;
        private int width_;

        public ControlForm Control
        {
            set { controlForm = value; }
        }

        public int SongIndex
        {
            get { return songIndex; }
            set { songIndex = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Verse
        {
            get { return verse; }
            set { verse = value; nextItemVerseLabel.Text = verse; }
        }

        public int Width_
        {
            get { return width_; }
            set {
                width_ = value; 
                nextItemVerseLabel.MaximumSize = new Size(width_, 0);

                //nextItemVerseLabel.MaximumSize = new Size(width_, 0);
            }
        }

        private void nextItemVerseLabel_DoubleClick(object sender, EventArgs e)
        {
            controlForm.SetSongProjectionItem(songIndex, index);
        }

        private void NextItemVersesControl_Resize(object sender, EventArgs e)
        {

        }
    }
}
