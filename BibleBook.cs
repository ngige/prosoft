﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProSoft
{
    class BibleBook
    {
        public string name;
        public int number;
        public int chaptersCount;
        public List<BibleChapter> chapters;

        public BibleBook(string bookName, int bookNumber, int chaptersCount, List<BibleChapter> chapters)
        {
            this.name = bookName;
            this.number = bookNumber;
            this.chaptersCount = chaptersCount;
            this.chapters = chapters;
        }

        public string Name
        {
            get { return name; }
        }

        public int Number
        {
            get { return number; }
        }

        public int ChaptersCount
        {
            get { return chaptersCount; }
        }

        public List<BibleChapter> Chapters
        {
            get { return chapters; }
        }
    }
}
