﻿
namespace ProSoft
{
    partial class ProjectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projectionPanel = new System.Windows.Forms.Panel();
            this.projectionBodyLabel = new System.Windows.Forms.Label();
            this.projectionFooterLabel = new System.Windows.Forms.Label();
            this.projectionHeaderLabel = new System.Windows.Forms.Label();
            this.projectionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // projectionPanel
            // 
            this.projectionPanel.Controls.Add(this.projectionBodyLabel);
            this.projectionPanel.Controls.Add(this.projectionFooterLabel);
            this.projectionPanel.Controls.Add(this.projectionHeaderLabel);
            this.projectionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionPanel.Location = new System.Drawing.Point(0, 0);
            this.projectionPanel.Name = "projectionPanel";
            this.projectionPanel.Size = new System.Drawing.Size(800, 450);
            this.projectionPanel.TabIndex = 0;
            // 
            // projectionBodyLabel
            // 
            this.projectionBodyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionBodyLabel.Location = new System.Drawing.Point(0, 24);
            this.projectionBodyLabel.Name = "projectionBodyLabel";
            this.projectionBodyLabel.Size = new System.Drawing.Size(800, 402);
            this.projectionBodyLabel.TabIndex = 1;
            this.projectionBodyLabel.Text = "label2";
            this.projectionBodyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // projectionFooterLabel
            // 
            this.projectionFooterLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.projectionFooterLabel.Location = new System.Drawing.Point(0, 426);
            this.projectionFooterLabel.Name = "projectionFooterLabel";
            this.projectionFooterLabel.Size = new System.Drawing.Size(800, 24);
            this.projectionFooterLabel.TabIndex = 2;
            this.projectionFooterLabel.Text = "label3";
            this.projectionFooterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // projectionHeaderLabel
            // 
            this.projectionHeaderLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.projectionHeaderLabel.Location = new System.Drawing.Point(0, 0);
            this.projectionHeaderLabel.Name = "projectionHeaderLabel";
            this.projectionHeaderLabel.Size = new System.Drawing.Size(800, 24);
            this.projectionHeaderLabel.TabIndex = 0;
            this.projectionHeaderLabel.Text = "label1";
            this.projectionHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProjectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.projectionPanel);
            this.Name = "ProjectionForm";
            this.Text = "ProjectionForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProjectionForm_FormClosing);
            this.projectionPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel projectionPanel;
        private System.Windows.Forms.Label projectionFooterLabel;
        private System.Windows.Forms.Label projectionBodyLabel;
        private System.Windows.Forms.Label projectionHeaderLabel;
    }
}