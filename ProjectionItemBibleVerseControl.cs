﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class ProjectionItemBibleVerseControl : UserControl
    {
        private int index;
        private ControlForm controlFrm;
        public string verse;

        public ProjectionItemBibleVerseControl(int index, ControlForm controlFrm, string verse)
        {
            this.index = index;
            this.controlFrm = controlFrm;
            this.verse = verse;

            InitializeComponent();

            verseLabel.Text = verse;
            ResizeLabel();
        }

        private void ResizeLabel()
        {
            verseLabel.MaximumSize = new Size(this.Width, 0);
        }

        private void ProjectiontemBibleVerseControl_Resize(object sender, EventArgs e)
        {
            ResizeLabel();
        }

        private void ProjectiontemBibleVerseControl_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("---------->>>>");

            controlFrm.Project(verse);
        }
    }
}
