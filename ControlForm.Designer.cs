﻿using System.Diagnostics;

namespace ProSoft
{
    partial class ControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlForm));
            this.projectButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ColumnOneSplitContainer = new System.Windows.Forms.SplitContainer();
            this.sheduleFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.openScheduleButton = new System.Windows.Forms.Button();
            this.saveScheduleButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SongsTabPage = new System.Windows.Forms.TabPage();
            this.songsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.searchSongTextBox = new System.Windows.Forms.TextBox();
            this.sortSongContextMenuStripLabel = new System.Windows.Forms.Label();
            this.addSongButton = new System.Windows.Forms.Button();
            this.bibleTabPage = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bibleOneVerseFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.bibleSearchChapterVerseTextBox = new System.Windows.Forms.TextBox();
            this.bibleSearchBookChapterTextBox = new System.Windows.Forms.TextBox();
            this.bibleSearchBookTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AddBibleButton = new System.Windows.Forms.Button();
            this.ColumnTwoSplitContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.nextItemFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.nextItemAuthorLabel = new System.Windows.Forms.Label();
            this.nextItemTitleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ColumnThreeSplitContainer = new System.Windows.Forms.SplitContainer();
            this.projectionItemFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sortSongContextMenuStripLabelToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.sortSongContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.titleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateAddedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topBarPanel = new System.Windows.Forms.Panel();
            this.alertPanel = new System.Windows.Forms.Panel();
            this.alertBodyLabel = new System.Windows.Forms.Label();
            this.alertHeaderLabel = new System.Windows.Forms.Label();
            this.alertCloseButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnOneSplitContainer)).BeginInit();
            this.ColumnOneSplitContainer.Panel1.SuspendLayout();
            this.ColumnOneSplitContainer.Panel2.SuspendLayout();
            this.ColumnOneSplitContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SongsTabPage.SuspendLayout();
            this.panel2.SuspendLayout();
            this.bibleTabPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnTwoSplitContainer)).BeginInit();
            this.ColumnTwoSplitContainer.Panel1.SuspendLayout();
            this.ColumnTwoSplitContainer.Panel2.SuspendLayout();
            this.ColumnTwoSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnThreeSplitContainer)).BeginInit();
            this.ColumnThreeSplitContainer.Panel1.SuspendLayout();
            this.ColumnThreeSplitContainer.SuspendLayout();
            this.sortSongContextMenuStrip.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.topBarPanel.SuspendLayout();
            this.alertPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // projectButton
            // 
            this.projectButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.projectButton.Location = new System.Drawing.Point(725, 0);
            this.projectButton.Name = "projectButton";
            this.projectButton.Size = new System.Drawing.Size(75, 25);
            this.projectButton.TabIndex = 0;
            this.projectButton.Text = "Go Live";
            this.projectButton.UseVisualStyleBackColor = true;
            this.projectButton.Click += new System.EventHandler(this.GoLiveButton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ColumnOneSplitContainer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ColumnTwoSplitContainer);
            this.splitContainer1.Size = new System.Drawing.Size(800, 425);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 1;
            // 
            // ColumnOneSplitContainer
            // 
            this.ColumnOneSplitContainer.BackColor = System.Drawing.SystemColors.Control;
            this.ColumnOneSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ColumnOneSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnOneSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ColumnOneSplitContainer.Name = "ColumnOneSplitContainer";
            this.ColumnOneSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ColumnOneSplitContainer.Panel1
            // 
            this.ColumnOneSplitContainer.Panel1.Controls.Add(this.sheduleFlowLayoutPanel);
            this.ColumnOneSplitContainer.Panel1.Controls.Add(this.panel1);
            this.ColumnOneSplitContainer.Panel1.Resize += new System.EventHandler(this.songsFlowLayoutPanel_Resize);
            // 
            // ColumnOneSplitContainer.Panel2
            // 
            this.ColumnOneSplitContainer.Panel2.Controls.Add(this.tabControl1);
            this.ColumnOneSplitContainer.Size = new System.Drawing.Size(253, 425);
            this.ColumnOneSplitContainer.SplitterDistance = 212;
            this.ColumnOneSplitContainer.TabIndex = 0;
            // 
            // sheduleFlowLayoutPanel
            // 
            this.sheduleFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sheduleFlowLayoutPanel.Location = new System.Drawing.Point(0, 25);
            this.sheduleFlowLayoutPanel.Name = "sheduleFlowLayoutPanel";
            this.sheduleFlowLayoutPanel.Size = new System.Drawing.Size(251, 185);
            this.sheduleFlowLayoutPanel.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.openScheduleButton);
            this.panel1.Controls.Add(this.saveScheduleButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 25);
            this.panel1.TabIndex = 0;
            // 
            // openScheduleButton
            // 
            this.openScheduleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.openScheduleButton.Location = new System.Drawing.Point(101, 0);
            this.openScheduleButton.Name = "openScheduleButton";
            this.openScheduleButton.Size = new System.Drawing.Size(75, 25);
            this.openScheduleButton.TabIndex = 1;
            this.openScheduleButton.Text = "Open";
            this.openScheduleButton.UseVisualStyleBackColor = true;
            this.openScheduleButton.Click += new System.EventHandler(this.openScheduleButton_Click);
            // 
            // saveScheduleButton
            // 
            this.saveScheduleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.saveScheduleButton.Location = new System.Drawing.Point(176, 0);
            this.saveScheduleButton.Name = "saveScheduleButton";
            this.saveScheduleButton.Size = new System.Drawing.Size(75, 25);
            this.saveScheduleButton.TabIndex = 0;
            this.saveScheduleButton.Text = "Save";
            this.saveScheduleButton.UseVisualStyleBackColor = true;
            this.saveScheduleButton.Click += new System.EventHandler(this.SaveScheduleButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.SongsTabPage);
            this.tabControl1.Controls.Add(this.bibleTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(251, 207);
            this.tabControl1.TabIndex = 0;
            // 
            // SongsTabPage
            // 
            this.SongsTabPage.Controls.Add(this.songsFlowLayoutPanel);
            this.SongsTabPage.Controls.Add(this.panel2);
            this.SongsTabPage.Controls.Add(this.addSongButton);
            this.SongsTabPage.Location = new System.Drawing.Point(4, 22);
            this.SongsTabPage.Name = "SongsTabPage";
            this.SongsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SongsTabPage.Size = new System.Drawing.Size(243, 181);
            this.SongsTabPage.TabIndex = 0;
            this.SongsTabPage.Text = "Songs";
            this.SongsTabPage.UseVisualStyleBackColor = true;
            // 
            // songsFlowLayoutPanel
            // 
            this.songsFlowLayoutPanel.AutoScroll = true;
            this.songsFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.songsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.songsFlowLayoutPanel.Location = new System.Drawing.Point(3, 27);
            this.songsFlowLayoutPanel.Name = "songsFlowLayoutPanel";
            this.songsFlowLayoutPanel.Padding = new System.Windows.Forms.Padding(0, 0, 100, 0);
            this.songsFlowLayoutPanel.Size = new System.Drawing.Size(237, 128);
            this.songsFlowLayoutPanel.TabIndex = 1;
            this.songsFlowLayoutPanel.WrapContents = false;
            this.songsFlowLayoutPanel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ControlForm_PreviewKeyDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.searchSongTextBox);
            this.panel2.Controls.Add(this.sortSongContextMenuStripLabel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(237, 24);
            this.panel2.TabIndex = 0;
            // 
            // searchSongTextBox
            // 
            this.searchSongTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchSongTextBox.Location = new System.Drawing.Point(0, 0);
            this.searchSongTextBox.Name = "searchSongTextBox";
            this.searchSongTextBox.Size = new System.Drawing.Size(213, 20);
            this.searchSongTextBox.TabIndex = 0;
            this.searchSongTextBox.WordWrap = false;
            this.searchSongTextBox.TextChanged += new System.EventHandler(this.searchSongTextBox_TextChanged);
            this.searchSongTextBox.Enter += new System.EventHandler(this.SearchSongTextBox_Enter);
            this.searchSongTextBox.Leave += new System.EventHandler(this.SearchSongTextBox_Leave);
            // 
            // sortSongContextMenuStripLabel
            // 
            this.sortSongContextMenuStripLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.sortSongContextMenuStripLabel.Image = global::ProSoft.Properties.Resources.sort_black;
            this.sortSongContextMenuStripLabel.Location = new System.Drawing.Point(213, 0);
            this.sortSongContextMenuStripLabel.Name = "sortSongContextMenuStripLabel";
            this.sortSongContextMenuStripLabel.Size = new System.Drawing.Size(24, 24);
            this.sortSongContextMenuStripLabel.TabIndex = 1;
            this.sortSongContextMenuStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sortSongContextMenuStripLabelToolTip.SetToolTip(this.sortSongContextMenuStripLabel, "Sort Song");
            this.sortSongContextMenuStripLabel.Click += new System.EventHandler(this.sortSongContextMenuStripLabel_Click);
            // 
            // addSongButton
            // 
            this.addSongButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addSongButton.Location = new System.Drawing.Point(3, 155);
            this.addSongButton.Name = "addSongButton";
            this.addSongButton.Size = new System.Drawing.Size(237, 23);
            this.addSongButton.TabIndex = 0;
            this.addSongButton.Text = "Add Song";
            this.addSongButton.UseVisualStyleBackColor = true;
            this.addSongButton.Click += new System.EventHandler(this.AddSongButton_Click);
            // 
            // bibleTabPage
            // 
            this.bibleTabPage.Controls.Add(this.tabControl2);
            this.bibleTabPage.Location = new System.Drawing.Point(4, 22);
            this.bibleTabPage.Name = "bibleTabPage";
            this.bibleTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.bibleTabPage.Size = new System.Drawing.Size(243, 181);
            this.bibleTabPage.TabIndex = 1;
            this.bibleTabPage.Text = "Bible";
            this.bibleTabPage.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(237, 175);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.bibleOneVerseFlowLayoutPanel);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(229, 149);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "KJV";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bibleOneVerseFlowLayoutPanel
            // 
            this.bibleOneVerseFlowLayoutPanel.AutoScroll = true;
            this.bibleOneVerseFlowLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bibleOneVerseFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bibleOneVerseFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.bibleOneVerseFlowLayoutPanel.Location = new System.Drawing.Point(3, 27);
            this.bibleOneVerseFlowLayoutPanel.Name = "bibleOneVerseFlowLayoutPanel";
            this.bibleOneVerseFlowLayoutPanel.Padding = new System.Windows.Forms.Padding(0, 0, 100, 0);
            this.bibleOneVerseFlowLayoutPanel.Size = new System.Drawing.Size(223, 119);
            this.bibleOneVerseFlowLayoutPanel.TabIndex = 2;
            this.bibleOneVerseFlowLayoutPanel.WrapContents = false;
            // 
            // panel3
            // 
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.bibleSearchChapterVerseTextBox);
            this.panel3.Controls.Add(this.bibleSearchBookChapterTextBox);
            this.panel3.Controls.Add(this.bibleSearchBookTextBox);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(223, 24);
            this.panel3.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.Location = new System.Drawing.Point(150, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 24);
            this.button1.TabIndex = 3;
            this.button1.Text = "Go";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // bibleSearchChapterVerseTextBox
            // 
            this.bibleSearchChapterVerseTextBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.bibleSearchChapterVerseTextBox.Location = new System.Drawing.Point(125, 0);
            this.bibleSearchChapterVerseTextBox.Name = "bibleSearchChapterVerseTextBox";
            this.bibleSearchChapterVerseTextBox.Size = new System.Drawing.Size(25, 20);
            this.bibleSearchChapterVerseTextBox.TabIndex = 2;
            this.bibleSearchChapterVerseTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bibleSearchChapterVerseTextBox_KeyUp);
            // 
            // bibleSearchBookChapterTextBox
            // 
            this.bibleSearchBookChapterTextBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.bibleSearchBookChapterTextBox.Location = new System.Drawing.Point(100, 0);
            this.bibleSearchBookChapterTextBox.Name = "bibleSearchBookChapterTextBox";
            this.bibleSearchBookChapterTextBox.Size = new System.Drawing.Size(25, 20);
            this.bibleSearchBookChapterTextBox.TabIndex = 4;
            this.bibleSearchBookChapterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bibleSearchBookChapterTextBox_KeyPress);
            this.bibleSearchBookChapterTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bibleSearchBookChapterTextBox_KeyUp);
            // 
            // bibleSearchBookTextBox
            // 
            this.bibleSearchBookTextBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.bibleSearchBookTextBox.Location = new System.Drawing.Point(0, 0);
            this.bibleSearchBookTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.bibleSearchBookTextBox.Name = "bibleSearchBookTextBox";
            this.bibleSearchBookTextBox.Size = new System.Drawing.Size(100, 20);
            this.bibleSearchBookTextBox.TabIndex = 0;
            this.bibleSearchBookTextBox.Enter += new System.EventHandler(this.bibleSearchBookTextBox_Enter);
            this.bibleSearchBookTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bibleSearchBookNameTextBox_KeyUp);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(229, 149);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.flowLayoutPanel1);
            this.tabPage3.Controls.Add(this.AddBibleButton);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(229, 149);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Add Bible";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.label6);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(223, 120);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Bible Json Format";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 299);
            this.label5.TabIndex = 2;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // AddBibleButton
            // 
            this.AddBibleButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.AddBibleButton.Location = new System.Drawing.Point(3, 123);
            this.AddBibleButton.Name = "AddBibleButton";
            this.AddBibleButton.Size = new System.Drawing.Size(223, 23);
            this.AddBibleButton.TabIndex = 0;
            this.AddBibleButton.Text = "Add Bible";
            this.AddBibleButton.UseVisualStyleBackColor = true;
            this.AddBibleButton.Click += new System.EventHandler(this.AddBibleButton_Click);
            // 
            // ColumnTwoSplitContainer
            // 
            this.ColumnTwoSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ColumnTwoSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnTwoSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ColumnTwoSplitContainer.Name = "ColumnTwoSplitContainer";
            // 
            // ColumnTwoSplitContainer.Panel1
            // 
            this.ColumnTwoSplitContainer.Panel1.Controls.Add(this.splitContainer4);
            // 
            // ColumnTwoSplitContainer.Panel2
            // 
            this.ColumnTwoSplitContainer.Panel2.Controls.Add(this.ColumnThreeSplitContainer);
            this.ColumnTwoSplitContainer.Size = new System.Drawing.Size(543, 425);
            this.ColumnTwoSplitContainer.SplitterDistance = 365;
            this.ColumnTwoSplitContainer.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.nextItemFlowLayoutPanel);
            this.splitContainer4.Panel1.Controls.Add(this.nextItemAuthorLabel);
            this.splitContainer4.Panel1.Controls.Add(this.nextItemTitleLabel);
            this.splitContainer4.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.BackColor = System.Drawing.Color.Black;
            this.splitContainer4.Size = new System.Drawing.Size(363, 423);
            this.splitContainer4.SplitterDistance = 210;
            this.splitContainer4.TabIndex = 0;
            // 
            // nextItemFlowLayoutPanel
            // 
            this.nextItemFlowLayoutPanel.AutoScroll = true;
            this.nextItemFlowLayoutPanel.AutoSize = true;
            this.nextItemFlowLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.nextItemFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextItemFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.nextItemFlowLayoutPanel.Location = new System.Drawing.Point(0, 38);
            this.nextItemFlowLayoutPanel.Name = "nextItemFlowLayoutPanel";
            this.nextItemFlowLayoutPanel.Size = new System.Drawing.Size(363, 172);
            this.nextItemFlowLayoutPanel.TabIndex = 3;
            this.nextItemFlowLayoutPanel.WrapContents = false;
            // 
            // nextItemAuthorLabel
            // 
            this.nextItemAuthorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nextItemAuthorLabel.Enabled = false;
            this.nextItemAuthorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextItemAuthorLabel.Location = new System.Drawing.Point(0, 28);
            this.nextItemAuthorLabel.Name = "nextItemAuthorLabel";
            this.nextItemAuthorLabel.Size = new System.Drawing.Size(363, 10);
            this.nextItemAuthorLabel.TabIndex = 1;
            // 
            // nextItemTitleLabel
            // 
            this.nextItemTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nextItemTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextItemTitleLabel.Location = new System.Drawing.Point(0, 15);
            this.nextItemTitleLabel.Name = "nextItemTitleLabel";
            this.nextItemTitleLabel.Size = new System.Drawing.Size(363, 13);
            this.nextItemTitleLabel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(363, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Next Item";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ColumnThreeSplitContainer
            // 
            this.ColumnThreeSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnThreeSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ColumnThreeSplitContainer.Name = "ColumnThreeSplitContainer";
            this.ColumnThreeSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ColumnThreeSplitContainer.Panel1
            // 
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.projectionItemFlowLayoutPanel);
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.label4);
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.label3);
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.label2);
            // 
            // ColumnThreeSplitContainer.Panel2
            // 
            this.ColumnThreeSplitContainer.Panel2.BackColor = System.Drawing.Color.Black;
            this.ColumnThreeSplitContainer.Size = new System.Drawing.Size(172, 423);
            this.ColumnThreeSplitContainer.SplitterDistance = 209;
            this.ColumnThreeSplitContainer.TabIndex = 0;
            // 
            // projectionItemFlowLayoutPanel
            // 
            this.projectionItemFlowLayoutPanel.AutoScroll = true;
            this.projectionItemFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionItemFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.projectionItemFlowLayoutPanel.Location = new System.Drawing.Point(0, 40);
            this.projectionItemFlowLayoutPanel.Name = "projectionItemFlowLayoutPanel";
            this.projectionItemFlowLayoutPanel.Size = new System.Drawing.Size(172, 169);
            this.projectionItemFlowLayoutPanel.TabIndex = 3;
            this.projectionItemFlowLayoutPanel.WrapContents = false;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 10);
            this.label4.TabIndex = 2;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "label3";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // sortSongContextMenuStrip
            // 
            this.sortSongContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.titleToolStripMenuItem,
            this.dateAddedToolStripMenuItem});
            this.sortSongContextMenuStrip.Name = "sortSongContextMenuStrip";
            this.sortSongContextMenuStrip.Size = new System.Drawing.Size(169, 48);
            // 
            // titleToolStripMenuItem
            // 
            this.titleToolStripMenuItem.Name = "titleToolStripMenuItem";
            this.titleToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.titleToolStripMenuItem.Text = "Sort Song by Title";
            this.titleToolStripMenuItem.Click += new System.EventHandler(this.sortSongByTitleToolStripMenuItem_Click);
            // 
            // dateAddedToolStripMenuItem
            // 
            this.dateAddedToolStripMenuItem.Name = "dateAddedToolStripMenuItem";
            this.dateAddedToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.dateAddedToolStripMenuItem.Text = "Sort Song by Date";
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(725, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.settingsToolStripMenuItem.Text = "settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // topBarPanel
            // 
            this.topBarPanel.Controls.Add(this.mainMenuStrip);
            this.topBarPanel.Controls.Add(this.projectButton);
            this.topBarPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topBarPanel.Location = new System.Drawing.Point(0, 0);
            this.topBarPanel.Name = "topBarPanel";
            this.topBarPanel.Size = new System.Drawing.Size(800, 25);
            this.topBarPanel.TabIndex = 2;
            // 
            // alertPanel
            // 
            this.alertPanel.BackColor = System.Drawing.Color.Bisque;
            this.alertPanel.Controls.Add(this.alertBodyLabel);
            this.alertPanel.Controls.Add(this.alertHeaderLabel);
            this.alertPanel.Controls.Add(this.alertCloseButton);
            this.alertPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.alertPanel.Location = new System.Drawing.Point(625, 25);
            this.alertPanel.MaximumSize = new System.Drawing.Size(200, 125);
            this.alertPanel.MinimumSize = new System.Drawing.Size(175, 100);
            this.alertPanel.Name = "alertPanel";
            this.alertPanel.Padding = new System.Windows.Forms.Padding(8);
            this.alertPanel.Size = new System.Drawing.Size(175, 125);
            this.alertPanel.TabIndex = 9;
            this.alertPanel.Visible = false;
            // 
            // alertBodyLabel
            // 
            this.alertBodyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alertBodyLabel.Location = new System.Drawing.Point(8, 32);
            this.alertBodyLabel.Name = "alertBodyLabel";
            this.alertBodyLabel.Size = new System.Drawing.Size(159, 62);
            this.alertBodyLabel.TabIndex = 0;
            this.alertBodyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // alertHeaderLabel
            // 
            this.alertHeaderLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.alertHeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alertHeaderLabel.Location = new System.Drawing.Point(8, 8);
            this.alertHeaderLabel.Name = "alertHeaderLabel";
            this.alertHeaderLabel.Size = new System.Drawing.Size(159, 24);
            this.alertHeaderLabel.TabIndex = 2;
            this.alertHeaderLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // alertCloseButton
            // 
            this.alertCloseButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.alertCloseButton.Location = new System.Drawing.Point(8, 94);
            this.alertCloseButton.Name = "alertCloseButton";
            this.alertCloseButton.Size = new System.Drawing.Size(159, 23);
            this.alertCloseButton.TabIndex = 1;
            this.alertCloseButton.Text = "Close";
            this.alertCloseButton.UseVisualStyleBackColor = true;
            this.alertCloseButton.Click += new System.EventHandler(this.alertCloseButton_Click);
            // 
            // ControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.alertPanel);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.topBarPanel);
            this.KeyPreview = true;
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "ControlForm";
            this.Text = "Control Dashboard";
            this.Load += new System.EventHandler(this.ControlForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ControlForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ControlForm_KeyUp);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ControlForm_PreviewKeyDown);
            this.Resize += new System.EventHandler(this.songsFlowLayoutPanel_Resize);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ColumnOneSplitContainer.Panel1.ResumeLayout(false);
            this.ColumnOneSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColumnOneSplitContainer)).EndInit();
            this.ColumnOneSplitContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.SongsTabPage.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.bibleTabPage.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ColumnTwoSplitContainer.Panel1.ResumeLayout(false);
            this.ColumnTwoSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColumnTwoSplitContainer)).EndInit();
            this.ColumnTwoSplitContainer.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ColumnThreeSplitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColumnThreeSplitContainer)).EndInit();
            this.ColumnThreeSplitContainer.ResumeLayout(false);
            this.sortSongContextMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.topBarPanel.ResumeLayout(false);
            this.topBarPanel.PerformLayout();
            this.alertPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.TabPage bibleTabPage;
        private System.Windows.Forms.SplitContainer ColumnOneSplitContainer;
        private System.Windows.Forms.SplitContainer ColumnThreeSplitContainer;
        private System.Windows.Forms.SplitContainer ColumnTwoSplitContainer;
        private System.Windows.Forms.Button projectButton;
        private System.Windows.Forms.TabPage SongsTabPage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TabControl tabControl1;

        #endregion

        private System.Windows.Forms.Label nextItemAuthorLabel;
        private System.Windows.Forms.Label nextItemTitleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel nextItemFlowLayoutPanel;
        private System.Windows.Forms.Button addSongButton;
        private System.Windows.Forms.FlowLayoutPanel songsFlowLayoutPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel projectionItemFlowLayoutPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button saveScheduleButton;
        private System.Windows.Forms.FlowLayoutPanel sheduleFlowLayoutPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox searchSongTextBox;
        private System.Windows.Forms.Label sortSongContextMenuStripLabel;
        private System.Windows.Forms.ToolTip sortSongContextMenuStripLabelToolTip;
        private System.Windows.Forms.ContextMenuStrip sortSongContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem titleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dateAddedToolStripMenuItem;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Panel topBarPanel;
        private System.Windows.Forms.Button openScheduleButton;
        private System.Windows.Forms.Panel alertPanel;
        private System.Windows.Forms.Label alertHeaderLabel;
        private System.Windows.Forms.Label alertBodyLabel;
        private System.Windows.Forms.Button alertCloseButton;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button AddBibleButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel bibleOneVerseFlowLayoutPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox bibleSearchBookTextBox;
        private System.Windows.Forms.TextBox bibleSearchChapterVerseTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox bibleSearchBookChapterTextBox;
    }
}

