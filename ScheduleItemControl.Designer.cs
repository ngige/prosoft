﻿
namespace ProSoft
{
    partial class ScheduleItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.scheduleItemControlContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activateScheduleItemControlContextMenuStripLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.songAuthorLabel = new System.Windows.Forms.Label();
            this.songTitleLabel = new System.Windows.Forms.Label();
            this.scheduleItemControlContextMenuStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // scheduleItemControlContextMenuStrip
            // 
            this.scheduleItemControlContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.scheduleItemControlContextMenuStrip.Name = "songListItemContextMenuStrip";
            this.scheduleItemControlContextMenuStrip.Size = new System.Drawing.Size(118, 48);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // activateScheduleItemControlContextMenuStripLabel
            // 
            this.activateScheduleItemControlContextMenuStripLabel.Image = global::ProSoft.Properties.Resources.dots_vertical_black;
            this.activateScheduleItemControlContextMenuStripLabel.Location = new System.Drawing.Point(334, 0);
            this.activateScheduleItemControlContextMenuStripLabel.Name = "activateScheduleItemControlContextMenuStripLabel";
            this.activateScheduleItemControlContextMenuStripLabel.Padding = new System.Windows.Forms.Padding(8, 3, 12, 4);
            this.activateScheduleItemControlContextMenuStripLabel.Size = new System.Drawing.Size(20, 20);
            this.activateScheduleItemControlContextMenuStripLabel.TabIndex = 5;
            this.activateScheduleItemControlContextMenuStripLabel.Click += new System.EventHandler(this.activateScheduleItemControlContextMenuStripLabel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.Controls.Add(this.activateScheduleItemControlContextMenuStripLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(377, 35);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.songAuthorLabel);
            this.panel3.Controls.Add(this.songTitleLabel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(331, 35);
            this.panel3.TabIndex = 7;
            // 
            // songAuthorLabel
            // 
            this.songAuthorLabel.AutoEllipsis = true;
            this.songAuthorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.songAuthorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.songAuthorLabel.Location = new System.Drawing.Point(0, 20);
            this.songAuthorLabel.Name = "songAuthorLabel";
            this.songAuthorLabel.Size = new System.Drawing.Size(331, 13);
            this.songAuthorLabel.TabIndex = 1;
            this.songAuthorLabel.Text = "Song author";
            this.songAuthorLabel.DoubleClick += new System.EventHandler(this.ScheduleItemControl_DoubleClick);
            // 
            // songTitleLabel
            // 
            this.songTitleLabel.AutoEllipsis = true;
            this.songTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.songTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.songTitleLabel.Location = new System.Drawing.Point(0, 0);
            this.songTitleLabel.MinimumSize = new System.Drawing.Size(100, 0);
            this.songTitleLabel.Name = "songTitleLabel";
            this.songTitleLabel.Size = new System.Drawing.Size(331, 20);
            this.songTitleLabel.TabIndex = 0;
            this.songTitleLabel.Text = "Song title";
            this.songTitleLabel.DoubleClick += new System.EventHandler(this.ScheduleItemControl_DoubleClick);
            // 
            // ScheduleItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ScheduleItemControl";
            this.Size = new System.Drawing.Size(377, 36);
            this.DoubleClick += new System.EventHandler(this.ScheduleItemControl_DoubleClick);
            this.scheduleItemControlContextMenuStrip.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip scheduleItemControlContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.Label activateScheduleItemControlContextMenuStripLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label songAuthorLabel;
        private System.Windows.Forms.Label songTitleLabel;
    }
}
