﻿namespace ProSoft
{
    partial class AddSongForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.songTitleTextBox = new System.Windows.Forms.TextBox();
            this.songVersesTextBox = new System.Windows.Forms.TextBox();
            this.saveSongButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.SongTitleLabel = new System.Windows.Forms.Label();
            this.songVersesLabel = new System.Windows.Forms.Label();
            this.songAuthorTextBox = new System.Windows.Forms.TextBox();
            this.songAuthorlabel = new System.Windows.Forms.Label();
            this.alertPanel = new System.Windows.Forms.Panel();
            this.alertLabel = new System.Windows.Forms.Label();
            this.duplicateSongButton = new System.Windows.Forms.Button();
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.updateSongButton = new System.Windows.Forms.Button();
            this.spacerPanel = new System.Windows.Forms.Panel();
            this.spacerPanel2 = new System.Windows.Forms.Panel();
            this.spacerPanel3 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.alertPanel.SuspendLayout();
            this.buttonsPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // songTitleTextBox
            // 
            this.songTitleTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.songTitleTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.songTitleTextBox.Location = new System.Drawing.Point(16, 29);
            this.songTitleTextBox.Margin = new System.Windows.Forms.Padding(3, 16, 3, 16);
            this.songTitleTextBox.Name = "songTitleTextBox";
            this.songTitleTextBox.Size = new System.Drawing.Size(452, 20);
            this.songTitleTextBox.TabIndex = 0;
            this.songTitleTextBox.Tag = "";
            // 
            // songVersesTextBox
            // 
            this.songVersesTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.songVersesTextBox.Location = new System.Drawing.Point(0, 0);
            this.songVersesTextBox.Margin = new System.Windows.Forms.Padding(16);
            this.songVersesTextBox.Multiline = true;
            this.songVersesTextBox.Name = "songVersesTextBox";
            this.songVersesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.songVersesTextBox.Size = new System.Drawing.Size(452, 277);
            this.songVersesTextBox.TabIndex = 1;
            // 
            // saveSongButton
            // 
            this.saveSongButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.saveSongButton.Location = new System.Drawing.Point(377, 0);
            this.saveSongButton.Name = "saveSongButton";
            this.saveSongButton.Size = new System.Drawing.Size(75, 25);
            this.saveSongButton.TabIndex = 3;
            this.saveSongButton.Text = "Save";
            this.saveSongButton.UseVisualStyleBackColor = true;
            this.saveSongButton.Click += new System.EventHandler(this.SaveSongButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(0, 0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 4;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // SongTitleLabel
            // 
            this.SongTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SongTitleLabel.Location = new System.Drawing.Point(16, 16);
            this.SongTitleLabel.Name = "SongTitleLabel";
            this.SongTitleLabel.Size = new System.Drawing.Size(452, 13);
            this.SongTitleLabel.TabIndex = 5;
            this.SongTitleLabel.Text = "Song Title*";
            // 
            // songVersesLabel
            // 
            this.songVersesLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.songVersesLabel.Location = new System.Drawing.Point(16, 65);
            this.songVersesLabel.Name = "songVersesLabel";
            this.songVersesLabel.Size = new System.Drawing.Size(452, 13);
            this.songVersesLabel.TabIndex = 6;
            this.songVersesLabel.Text = "Song Verse(s)*";
            // 
            // songAuthorTextBox
            // 
            this.songAuthorTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.songAuthorTextBox.Location = new System.Drawing.Point(16, 384);
            this.songAuthorTextBox.Name = "songAuthorTextBox";
            this.songAuthorTextBox.Size = new System.Drawing.Size(452, 20);
            this.songAuthorTextBox.TabIndex = 2;
            // 
            // songAuthorlabel
            // 
            this.songAuthorlabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.songAuthorlabel.Location = new System.Drawing.Point(16, 371);
            this.songAuthorlabel.Name = "songAuthorlabel";
            this.songAuthorlabel.Size = new System.Drawing.Size(452, 13);
            this.songAuthorlabel.TabIndex = 7;
            this.songAuthorlabel.Text = "Song Author";
            // 
            // alertPanel
            // 
            this.alertPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alertPanel.Controls.Add(this.alertLabel);
            this.alertPanel.Location = new System.Drawing.Point(268, 26);
            this.alertPanel.Name = "alertPanel";
            this.alertPanel.Size = new System.Drawing.Size(200, 100);
            this.alertPanel.TabIndex = 8;
            this.alertPanel.Visible = false;
            // 
            // alertLabel
            // 
            this.alertLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alertLabel.Location = new System.Drawing.Point(0, 0);
            this.alertLabel.Name = "alertLabel";
            this.alertLabel.Size = new System.Drawing.Size(200, 100);
            this.alertLabel.TabIndex = 0;
            // 
            // duplicateSongButton
            // 
            this.duplicateSongButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.duplicateSongButton.Location = new System.Drawing.Point(211, 0);
            this.duplicateSongButton.Name = "duplicateSongButton";
            this.duplicateSongButton.Size = new System.Drawing.Size(75, 25);
            this.duplicateSongButton.TabIndex = 9;
            this.duplicateSongButton.Text = "Duplicate";
            this.duplicateSongButton.UseVisualStyleBackColor = true;
            this.duplicateSongButton.Visible = false;
            this.duplicateSongButton.Click += new System.EventHandler(this.SaveSongButton_Click);
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Controls.Add(this.duplicateSongButton);
            this.buttonsPanel.Controls.Add(this.panel4);
            this.buttonsPanel.Controls.Add(this.updateSongButton);
            this.buttonsPanel.Controls.Add(this.saveSongButton);
            this.buttonsPanel.Controls.Add(this.closeButton);
            this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonsPanel.Location = new System.Drawing.Point(16, 420);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(452, 25);
            this.buttonsPanel.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(286, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(16, 25);
            this.panel4.TabIndex = 14;
            // 
            // updateSongButton
            // 
            this.updateSongButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.updateSongButton.Location = new System.Drawing.Point(302, 0);
            this.updateSongButton.Name = "updateSongButton";
            this.updateSongButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.updateSongButton.Size = new System.Drawing.Size(75, 25);
            this.updateSongButton.TabIndex = 10;
            this.updateSongButton.Text = "Update";
            this.updateSongButton.UseVisualStyleBackColor = true;
            this.updateSongButton.Visible = false;
            this.updateSongButton.Click += new System.EventHandler(this.updateSongButton_Click);
            // 
            // spacerPanel
            // 
            this.spacerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.spacerPanel.Location = new System.Drawing.Point(16, 49);
            this.spacerPanel.Name = "spacerPanel";
            this.spacerPanel.Size = new System.Drawing.Size(452, 16);
            this.spacerPanel.TabIndex = 11;
            // 
            // spacerPanel2
            // 
            this.spacerPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.spacerPanel2.Location = new System.Drawing.Point(16, 355);
            this.spacerPanel2.Name = "spacerPanel2";
            this.spacerPanel2.Size = new System.Drawing.Size(452, 16);
            this.spacerPanel2.TabIndex = 12;
            // 
            // spacerPanel3
            // 
            this.spacerPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.spacerPanel3.Location = new System.Drawing.Point(16, 404);
            this.spacerPanel3.Name = "spacerPanel3";
            this.spacerPanel3.Size = new System.Drawing.Size(452, 16);
            this.spacerPanel3.TabIndex = 13;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.songVersesTextBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(16, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 277);
            this.panel1.TabIndex = 15;
            // 
            // AddSongForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.alertPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.spacerPanel2);
            this.Controls.Add(this.songAuthorlabel);
            this.Controls.Add(this.songAuthorTextBox);
            this.Controls.Add(this.spacerPanel3);
            this.Controls.Add(this.buttonsPanel);
            this.Controls.Add(this.songVersesLabel);
            this.Controls.Add(this.spacerPanel);
            this.Controls.Add(this.songTitleTextBox);
            this.Controls.Add(this.SongTitleLabel);
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "AddSongForm";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Text = "Add New Song";
            this.Resize += new System.EventHandler(this.AddSongForm_Resize);
            this.alertPanel.ResumeLayout(false);
            this.buttonsPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox songTitleTextBox;
        private System.Windows.Forms.TextBox songVersesTextBox;
        private System.Windows.Forms.Button saveSongButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label SongTitleLabel;
        private System.Windows.Forms.Label songVersesLabel;
        private System.Windows.Forms.TextBox songAuthorTextBox;
        private System.Windows.Forms.Label songAuthorlabel;
        private System.Windows.Forms.Panel alertPanel;
        private System.Windows.Forms.Label alertLabel;
        private System.Windows.Forms.Button duplicateSongButton;
        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.Button updateSongButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel spacerPanel;
        private System.Windows.Forms.Panel spacerPanel2;
        private System.Windows.Forms.Panel spacerPanel3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel1;
    }
}