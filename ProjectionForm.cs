﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class ProjectionForm : Form
    {
        public ProjectionForm()
        {
            InitializeComponent();

            GetSetProjectSettings();
        }

        private void GetSetProjectSettings()
        {
            /*
             * Panel settings
             */
            projectionPanel.Padding = new Padding(
                Properties.Settings.Default.ProjectionPanelPaddingLeft,
                Properties.Settings.Default.ProjectionPanelPaddingTop,
                Properties.Settings.Default.ProjectionPanelPaddingRight,
                Properties.Settings.Default.ProjectionPanelPaddingBottom);

            projectionPanel.BackColor = Properties.Settings.Default.ProjectionPanelBackColor;
            projectionPanel.ForeColor = Properties.Settings.Default.ProjectionPanelForeColor;

            string fontFamily = Properties.Settings.Default.ProjectionFontFamily;
            if (fontFamily == "") fontFamily = this.Font.FontFamily.Name;

            float projectionPanelFontSize = Properties.Settings.Default.ProjectionPanelFontSize;
            if (projectionPanelFontSize == 0) projectionPanelFontSize = this.Font.Size;

            projectionPanel.Font = new Font(fontFamily, projectionPanelFontSize);

            projectionHeaderLabel.Font = new Font(fontFamily, projectionPanelFontSize);
            projectionFooterLabel.Font = new Font(fontFamily, projectionPanelFontSize);

            projectionHeaderLabel.Size = new Size(projectionHeaderLabel.Size.Width, (int)projectionPanelFontSize * 2);
            projectionFooterLabel.Size = new Size(projectionFooterLabel.Size.Width, (int)projectionPanelFontSize * 2);

            /*
             * Body settings
             */
            projectionBodyLabel.BackColor = Properties.Settings.Default.ProjectionBodyBackColor;
            projectionBodyLabel.ForeColor = Properties.Settings.Default.ProjectionBodyForeColor;

            float projectionBodyFontSize = Properties.Settings.Default.ProjectionBodyFontSize;
            if (projectionBodyFontSize == 0) projectionBodyFontSize = this.Font.Size;
            projectionBodyLabel.Font = new Font(projectionBodyLabel.Font.FontFamily, projectionBodyFontSize);

            fontFamily = Properties.Settings.Default.ProjectionFontFamily;
            if (fontFamily == "") fontFamily = this.Font.FontFamily.Name;

            projectionBodyLabel.Font = new Font(fontFamily, projectionBodyLabel.Font.Size);

            // Text align
            switch (Properties.Settings.Default.ProjectionBodyLabelTextAlign)
            {
                case "Top Center": projectionBodyLabel.TextAlign = ContentAlignment.TopCenter; break;
                case "Top Left": projectionBodyLabel.TextAlign = ContentAlignment.TopLeft; break;
                case "Top Right": projectionBodyLabel.TextAlign = ContentAlignment.TopRight; break;
                case "Middle Center": projectionBodyLabel.TextAlign = ContentAlignment.MiddleCenter; break;
                case "Middle Left": projectionBodyLabel.TextAlign = ContentAlignment.MiddleLeft; break;
                case "Middle Right": projectionBodyLabel.TextAlign = ContentAlignment.MiddleRight; break;
                case "Bottom Center": projectionBodyLabel.TextAlign = ContentAlignment.BottomCenter; break;
                case "Bottom Left": projectionBodyLabel.TextAlign = ContentAlignment.BottomLeft; break;
                case "Bottom Right": projectionBodyLabel.TextAlign = ContentAlignment.BottomRight; break;

                default: projectionBodyLabel.TextAlign = ContentAlignment.MiddleCenter; break;
            };
        }

        private void AdjustFontSizeToFit(Label projectionBodyLabel, float currentFontSize, float multiplier)
        {
            string text = projectionBodyLabel.Text;

            if (multiplier <= 0) multiplier = 1f;

            float height = projectionBodyLabel.Size.Height;
            height -= (projectionBodyLabel.Padding.Top + projectionBodyLabel.Padding.Bottom);

            float width = projectionBodyLabel.Size.Width;
            width -= (projectionBodyLabel.Padding.Left + projectionBodyLabel.Padding.Right);

            float area = height * width;

            float numberOfCharacters = text.Length;
            float maxFontSize = (float)Math.Sqrt((area / numberOfCharacters)) * multiplier;
            if (maxFontSize <= 0) maxFontSize = 1f;

            if (currentFontSize > maxFontSize) projectionBodyLabel.Font = new Font(projectionBodyLabel.Font.FontFamily, maxFontSize);
        }

        private void LogI(string info)
        {
            Debug.WriteLine("ProjectionForm: " + info);
        }

        internal void Project(string verse)
        {
            projectionBodyLabel.Text = verse;
            AdjustFontSizeToFit(projectionBodyLabel, projectionBodyLabel.Font.Size, Properties.Settings.Default.ProjectionBodyFontSizeMultpilier);
        }

        internal void UpdateUI()
        {
            GetSetProjectSettings();

            AdjustFontSizeToFit(projectionBodyLabel, projectionBodyLabel.Font.Size, Properties.Settings.Default.ProjectionBodyFontSizeMultpilier);
        }

        private void ProjectionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true; // Cancel close event.
        }
    }
}
