﻿
namespace ProSoft
{
    partial class ControlFormBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sheduleFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.projectionItemFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ColumnThreeSplitContainer = new System.Windows.Forms.SplitContainer();
            this.nextItemVersesFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.nextItemAuthorLabel = new System.Windows.Forms.Label();
            this.nextItemTitleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.ColumnTwoSplitContainer = new System.Windows.Forms.SplitContainer();
            this.BibleTabPage = new System.Windows.Forms.TabPage();
            this.songsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.addSongButton = new System.Windows.Forms.Button();
            this.SongsTabPage = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveScheduleButton = new System.Windows.Forms.Button();
            this.ColumnOneSplitContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.projectButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnThreeSplitContainer)).BeginInit();
            this.ColumnThreeSplitContainer.Panel1.SuspendLayout();
            this.ColumnThreeSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnTwoSplitContainer)).BeginInit();
            this.ColumnTwoSplitContainer.Panel1.SuspendLayout();
            this.ColumnTwoSplitContainer.Panel2.SuspendLayout();
            this.ColumnTwoSplitContainer.SuspendLayout();
            this.SongsTabPage.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ColumnOneSplitContainer)).BeginInit();
            this.ColumnOneSplitContainer.Panel1.SuspendLayout();
            this.ColumnOneSplitContainer.Panel2.SuspendLayout();
            this.ColumnOneSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sheduleFlowLayoutPanel
            // 
            this.sheduleFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sheduleFlowLayoutPanel.Location = new System.Drawing.Point(0, 25);
            this.sheduleFlowLayoutPanel.Name = "sheduleFlowLayoutPanel";
            this.sheduleFlowLayoutPanel.Size = new System.Drawing.Size(251, 199);
            this.sheduleFlowLayoutPanel.TabIndex = 1;
            // 
            // projectionItemFlowLayoutPanel
            // 
            this.projectionItemFlowLayoutPanel.AutoScroll = true;
            this.projectionItemFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionItemFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.projectionItemFlowLayoutPanel.Location = new System.Drawing.Point(0, 40);
            this.projectionItemFlowLayoutPanel.Name = "projectionItemFlowLayoutPanel";
            this.projectionItemFlowLayoutPanel.Size = new System.Drawing.Size(172, 182);
            this.projectionItemFlowLayoutPanel.TabIndex = 3;
            this.projectionItemFlowLayoutPanel.WrapContents = false;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 10);
            this.label4.TabIndex = 2;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "label3";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // ColumnThreeSplitContainer
            // 
            this.ColumnThreeSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnThreeSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ColumnThreeSplitContainer.Name = "ColumnThreeSplitContainer";
            this.ColumnThreeSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ColumnThreeSplitContainer.Panel1
            // 
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.projectionItemFlowLayoutPanel);
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.label4);
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.label3);
            this.ColumnThreeSplitContainer.Panel1.Controls.Add(this.label2);
            // 
            // ColumnThreeSplitContainer.Panel2
            // 
            this.ColumnThreeSplitContainer.Panel2.BackColor = System.Drawing.Color.Black;
            this.ColumnThreeSplitContainer.Size = new System.Drawing.Size(172, 448);
            this.ColumnThreeSplitContainer.SplitterDistance = 222;
            this.ColumnThreeSplitContainer.TabIndex = 0;
            // 
            // nextItemVersesFlowLayoutPanel
            // 
            this.nextItemVersesFlowLayoutPanel.AutoScroll = true;
            this.nextItemVersesFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextItemVersesFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.nextItemVersesFlowLayoutPanel.Location = new System.Drawing.Point(0, 38);
            this.nextItemVersesFlowLayoutPanel.Name = "nextItemVersesFlowLayoutPanel";
            this.nextItemVersesFlowLayoutPanel.Size = new System.Drawing.Size(363, 185);
            this.nextItemVersesFlowLayoutPanel.TabIndex = 3;
            this.nextItemVersesFlowLayoutPanel.WrapContents = false;
            // 
            // nextItemAuthorLabel
            // 
            this.nextItemAuthorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nextItemAuthorLabel.Enabled = false;
            this.nextItemAuthorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextItemAuthorLabel.Location = new System.Drawing.Point(0, 28);
            this.nextItemAuthorLabel.Name = "nextItemAuthorLabel";
            this.nextItemAuthorLabel.Size = new System.Drawing.Size(363, 10);
            this.nextItemAuthorLabel.TabIndex = 1;
            // 
            // nextItemTitleLabel
            // 
            this.nextItemTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nextItemTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextItemTitleLabel.Location = new System.Drawing.Point(0, 15);
            this.nextItemTitleLabel.Name = "nextItemTitleLabel";
            this.nextItemTitleLabel.Size = new System.Drawing.Size(363, 13);
            this.nextItemTitleLabel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(363, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Next Item";
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.nextItemVersesFlowLayoutPanel);
            this.splitContainer4.Panel1.Controls.Add(this.nextItemAuthorLabel);
            this.splitContainer4.Panel1.Controls.Add(this.nextItemTitleLabel);
            this.splitContainer4.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.BackColor = System.Drawing.Color.Black;
            this.splitContainer4.Size = new System.Drawing.Size(363, 448);
            this.splitContainer4.SplitterDistance = 223;
            this.splitContainer4.TabIndex = 0;
            // 
            // ColumnTwoSplitContainer
            // 
            this.ColumnTwoSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ColumnTwoSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnTwoSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ColumnTwoSplitContainer.Name = "ColumnTwoSplitContainer";
            // 
            // ColumnTwoSplitContainer.Panel1
            // 
            this.ColumnTwoSplitContainer.Panel1.Controls.Add(this.splitContainer4);
            // 
            // ColumnTwoSplitContainer.Panel2
            // 
            this.ColumnTwoSplitContainer.Panel2.Controls.Add(this.ColumnThreeSplitContainer);
            this.ColumnTwoSplitContainer.Size = new System.Drawing.Size(543, 450);
            this.ColumnTwoSplitContainer.SplitterDistance = 365;
            this.ColumnTwoSplitContainer.TabIndex = 0;
            // 
            // BibleTabPage
            // 
            this.BibleTabPage.Location = new System.Drawing.Point(4, 22);
            this.BibleTabPage.Name = "BibleTabPage";
            this.BibleTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.BibleTabPage.Size = new System.Drawing.Size(243, 192);
            this.BibleTabPage.TabIndex = 1;
            this.BibleTabPage.Text = "tabPage2";
            this.BibleTabPage.UseVisualStyleBackColor = true;
            // 
            // songsFlowLayoutPanel
            // 
            this.songsFlowLayoutPanel.AutoScroll = true;
            this.songsFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.songsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.songsFlowLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.songsFlowLayoutPanel.Name = "songsFlowLayoutPanel";
            this.songsFlowLayoutPanel.Padding = new System.Windows.Forms.Padding(0, 0, 100, 0);
            this.songsFlowLayoutPanel.Size = new System.Drawing.Size(237, 163);
            this.songsFlowLayoutPanel.TabIndex = 1;
            this.songsFlowLayoutPanel.WrapContents = false;
            // 
            // addSongButton
            // 
            this.addSongButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.addSongButton.Location = new System.Drawing.Point(3, 166);
            this.addSongButton.Name = "addSongButton";
            this.addSongButton.Size = new System.Drawing.Size(237, 23);
            this.addSongButton.TabIndex = 0;
            this.addSongButton.Text = "Add Song";
            this.addSongButton.UseVisualStyleBackColor = true;
            // 
            // SongsTabPage
            // 
            this.SongsTabPage.Controls.Add(this.songsFlowLayoutPanel);
            this.SongsTabPage.Controls.Add(this.addSongButton);
            this.SongsTabPage.Location = new System.Drawing.Point(4, 22);
            this.SongsTabPage.Name = "SongsTabPage";
            this.SongsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SongsTabPage.Size = new System.Drawing.Size(243, 192);
            this.SongsTabPage.TabIndex = 0;
            this.SongsTabPage.Text = "tabPage1";
            this.SongsTabPage.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.SongsTabPage);
            this.tabControl1.Controls.Add(this.BibleTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(251, 218);
            this.tabControl1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.saveScheduleButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 25);
            this.panel1.TabIndex = 0;
            // 
            // saveScheduleButton
            // 
            this.saveScheduleButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.saveScheduleButton.Location = new System.Drawing.Point(176, 0);
            this.saveScheduleButton.Name = "saveScheduleButton";
            this.saveScheduleButton.Size = new System.Drawing.Size(75, 25);
            this.saveScheduleButton.TabIndex = 0;
            this.saveScheduleButton.Text = "Save";
            this.saveScheduleButton.UseVisualStyleBackColor = true;
            // 
            // ColumnOneSplitContainer
            // 
            this.ColumnOneSplitContainer.BackColor = System.Drawing.SystemColors.Control;
            this.ColumnOneSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ColumnOneSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColumnOneSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ColumnOneSplitContainer.Name = "ColumnOneSplitContainer";
            this.ColumnOneSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ColumnOneSplitContainer.Panel1
            // 
            this.ColumnOneSplitContainer.Panel1.Controls.Add(this.sheduleFlowLayoutPanel);
            this.ColumnOneSplitContainer.Panel1.Controls.Add(this.panel1);
            this.ColumnOneSplitContainer.Panel1.Resize += new System.EventHandler(this.songsFlowLayoutPanel_Resize);
            // 
            // ColumnOneSplitContainer.Panel2
            // 
            this.ColumnOneSplitContainer.Panel2.Controls.Add(this.tabControl1);
            this.ColumnOneSplitContainer.Size = new System.Drawing.Size(253, 450);
            this.ColumnOneSplitContainer.SplitterDistance = 226;
            this.ColumnOneSplitContainer.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ColumnOneSplitContainer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ColumnTwoSplitContainer);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 3;
            // 
            // projectButton
            // 
            this.projectButton.Location = new System.Drawing.Point(713, 12);
            this.projectButton.Name = "projectButton";
            this.projectButton.Size = new System.Drawing.Size(75, 23);
            this.projectButton.TabIndex = 2;
            this.projectButton.Text = "Go Live";
            this.projectButton.UseVisualStyleBackColor = true;
            // 
            // ControlFormBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.projectButton);
            this.Name = "ControlFormBackup";
            this.Text = "ControlFormBackup";
            this.ColumnThreeSplitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColumnThreeSplitContainer)).EndInit();
            this.ColumnThreeSplitContainer.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ColumnTwoSplitContainer.Panel1.ResumeLayout(false);
            this.ColumnTwoSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColumnTwoSplitContainer)).EndInit();
            this.ColumnTwoSplitContainer.ResumeLayout(false);
            this.SongsTabPage.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ColumnOneSplitContainer.Panel1.ResumeLayout(false);
            this.ColumnOneSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ColumnOneSplitContainer)).EndInit();
            this.ColumnOneSplitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel sheduleFlowLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel projectionItemFlowLayoutPanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer ColumnThreeSplitContainer;
        private System.Windows.Forms.FlowLayoutPanel nextItemVersesFlowLayoutPanel;
        private System.Windows.Forms.Label nextItemAuthorLabel;
        private System.Windows.Forms.Label nextItemTitleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer ColumnTwoSplitContainer;
        private System.Windows.Forms.TabPage BibleTabPage;
        private System.Windows.Forms.FlowLayoutPanel songsFlowLayoutPanel;
        private System.Windows.Forms.Button addSongButton;
        private System.Windows.Forms.TabPage SongsTabPage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button saveScheduleButton;
        private System.Windows.Forms.SplitContainer ColumnOneSplitContainer;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button projectButton;
    }
}