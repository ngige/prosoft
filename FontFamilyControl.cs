﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProSoft
{
    public partial class FontFamilyControl : UserControl
    {
        private string text;

        public FontFamilyControl()
        {
            InitializeComponent();
        }

        public FontFamilyControl(string text)
        {
            this.text = text;
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public override string ToString()
        {
            return this.Text;
        }
    }
}
