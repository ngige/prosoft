﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace ProSoft
{
    public partial class AddSongForm : Form
    {
        delegate void SetTextCallback(string text);

        private ControlForm controlForm;
        private bool songSaved;
        private bool editSongBool = false;
        private int songItemControlindex;
        private int scheduleItemControlIndex;

        public AddSongForm(ControlForm cForm)
        {
            controlForm = cForm;

            InitializeComponent();

            songTitleTextBox.Text = controlForm.activeSongTitle;
            songVersesTextBox.Text = controlForm.activeSongVerses;
            songAuthorTextBox.Text = controlForm.activeSongAuthor;
        }

        internal void editSong(SongItemControl songItemControl, int scheduleItemControlIndex)
        {
            editSongBool = true;
            this.songItemControlindex = songItemControl.Index;
            this.scheduleItemControlIndex = scheduleItemControlIndex;
            songTitleTextBox.Text = songItemControl.Title;
            songVersesTextBox.Text = songItemControl.Verses;
            songAuthorTextBox.Text = songItemControl.Author;

            saveSongButton.Visible = false;
            updateSongButton.Visible = true;
            if(scheduleItemControlIndex == -1) duplicateSongButton.Visible = true;

            this.ShowDialog();
        }

        /*private void AddSongTitleTextBox_Enter(object sender, EventArgs e)
        {
            if (songTitleTextBox.Text == "Song Title")
            {
                songTitleTextBox.Text = "";
                songTitleTextBox.ForeColor = Color.Black;
            }
        }

        private void AddSongTitleTextBox_Leave(object sender, EventArgs e)
        {
            if (songTitleTextBox.Text == "")
            {
                songTitleTextBox.Text = "Song Title";
                songTitleTextBox.ForeColor = Color.Silver;
            }
        }*/



        private bool isSongValid()
        {
            bool isSongValid = true;

            if (songTitleTextBox.Text == "")
            {
                SongTitleLabel.Text = "Song Title Required";
                SongTitleLabel.ForeColor = Color.Red;
                isSongValid = false;
            }
            else
            {
                SongTitleLabel.Text = "Song Title*";
                SongTitleLabel.ForeColor = SystemColors.WindowText;
            }

            if (songVersesLabel.Text == "")
            {
                songVersesLabel.Text = "Song Verse(s) Required";
                songVersesLabel.ForeColor = Color.Red;
                isSongValid = false;
            }
            else
            {
                songVersesLabel.Text = "Song Verse(s)*";
                songVersesLabel.ForeColor = SystemColors.WindowText;
            }

            return isSongValid;
        }

        public void SongSaved(bool saved)
        {
            if (saved)
            {
                Alert(SongTitleLabel.Text + " Song saved");
                songSaved = true;
            } else
            {
                Alert(SongTitleLabel.Text + " Song not saved");
                songSaved = false;
            }
        }

        public void Alert(string message)
        {
            alertLabel.Text = message;
            alertPanel.Visible = true;

            Task.Delay(3000).ContinueWith(t => CloseAlert());
        }

        private void SetText(string text)
        {
            alertPanel.Visible = false;
        }

        public void CloseAlert()
        {
            if (alertPanel.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] {" (Invoke)" });
            }
            else
            {
                // It's on the same thread, no need for Invoke
                alertPanel.Visible = false;
            }
        }

        /*
         * Events
         */
        private void CloseButton_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("AddSongForm CloseButton_ClicksongSaved: " + songSaved);

            if (songSaved)
            {
                Debug.WriteLine("AddSongForm CloseButton_ClicksongSaved ++: " + songSaved);
                controlForm.activeSongTitle = "";
                controlForm.activeSongVerses = "";
                controlForm.activeSongAuthor = "";

                songTitleTextBox.Text = songVersesTextBox.Text = songAuthorTextBox.Text = "";
            } else
            {
                controlForm.activeSongTitle = songTitleTextBox.Text;
                controlForm.activeSongVerses = songVersesTextBox.Text;
                controlForm.activeSongAuthor = songAuthorTextBox.Text;
            }

            Close();
        }

        private void SaveSongButton_Click(object sender, EventArgs e)
        {
            if (isSongValid())
            {
                int songControlListIndex = controlForm.DB_InsertSong(new Song(songTitleTextBox.Text, songVersesTextBox.Text, songAuthorTextBox.Text));
                if (songControlListIndex > -1)
                {
                    songTitleTextBox.Text = controlForm.songItemControlList[songControlListIndex].Title;
                    Alert("Song Saved");
                }
                else Alert("Song Not Saved");
            }
        }

        private void updateSongButton_Click(object sender, EventArgs e)
        {
            if (isSongValid())
            {
               controlForm.DB_UpdateSong(songItemControlindex, scheduleItemControlIndex, songTitleTextBox.Text, songVersesTextBox.Text, songAuthorTextBox.Text);
            }
        }

        private void AddSongForm_Resize(object sender, EventArgs e)
        {
            songVersesTextBox.ScrollBars = ScrollBars.Vertical;
        }
    }
}
