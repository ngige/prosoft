﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProSoft
{
    public class Song
    {
        public string title;
        public string verses;
        public string author;

        public Song(string title, string verses, string author)
        {
            this.title = title;
            this.verses = verses;
            this.author = author;
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Verses
        {
            get { return verses; }
            set { verses = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }
    }
}
