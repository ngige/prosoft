﻿
namespace ProSoft
{
    partial class ProjectionItemsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projectionItemControlLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // projectionItemControlLabel
            // 
            this.projectionItemControlLabel.AutoSize = true;
            this.projectionItemControlLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectionItemControlLabel.Location = new System.Drawing.Point(0, 0);
            this.projectionItemControlLabel.Name = "projectionItemControlLabel";
            this.projectionItemControlLabel.Size = new System.Drawing.Size(0, 13);
            this.projectionItemControlLabel.TabIndex = 0;
            this.projectionItemControlLabel.Click += new System.EventHandler(this.ProjectionItemControlLabel_Click);
            // 
            // ProjectionItemsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.projectionItemControlLabel);
            this.Name = "ProjectionItemsControl";
            this.Size = new System.Drawing.Size(0, 13);
            this.Click += new System.EventHandler(this.ProjectionItemControlLabel_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label projectionItemControlLabel;
    }
}
