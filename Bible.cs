﻿using System.Collections.Generic;

namespace ProSoft
{
    public class Verse
    {
        public int number { get; set; }
        public string verse { get; set; }
    }

    public class Chapter
    {
        public int number { get; set; }
        public int versesCount { get; set; }
        public List<Verse> verses { get; set; }
    }

    public class Book
    {
        public string name { get; set; }
        public int number { get; set; }
        public int chaptersCount { get; set; }
        public List<Chapter> chapters { get; set; }
    }

    public class Bible
    {
        public string version { get; set; }
        public List<Book> books { get; set; }
    }
}
