﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProSoft
{
    class BibleChapter
    {
        public int number;
        public int versesCount;
        public List<BibleVerse> bibleVerses;

        public BibleChapter(int chapterNumber, int versesCount, List<BibleVerse> bibleVerses)
        {
            this.number = chapterNumber;
            this.versesCount = versesCount;
            this.bibleVerses = bibleVerses;
        }

        public int Number
        {
            get { return number; }
        }

        public int VersesCount
        {
            get { return versesCount; }
        }

        public List<BibleVerse> BibleVerses
        {
            get { return bibleVerses; }
        }
    }
}
